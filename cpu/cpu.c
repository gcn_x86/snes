#include "cpu.h"
#include "../ppu.h"
#include "../cart.h"
#include "../mem.h"
#include "uop.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <ctype.h>

unsigned long step_count = 0;

struct cpu_state_t cpu_state = {
//    .in_irqb = 1,
//    .in_rdy = 1,
//    .in_resb = 1,
//    .in_abortb = 1,
//    .in_nmib = 1,
    .regs = (struct reg_t []) {
        [REG_ACCUM]         = { .flag = STATUS_FLAG_M },
        [REG_X]             = { .flag = STATUS_FLAG_X },
        [REG_Y]             = { .flag = STATUS_FLAG_X },
        [REG_D]             = { },
        [REG_S]             = { },
        [REG_PBR]           = { },
        [REG_DBR]           = { },
        [REG_INST]          = { },
        [REG_PC]            = { },
        // [REG_P]             = { .flag = 0x0000},
        [REG_ADDR]          = { },
        [REG_BANK]          = { },
        [REG_ZERO]          = { },
    },
};

uint32_t mem_speed_map[][2] = {
    {MEM_SPEED_MED_CYCLES,  MEM_SPEED_FAST_CYCLES},
    {MEM_SPEED_MED_CYCLES,  MEM_SPEED_MED_CYCLES},
    {MEM_SPEED_FAST_CYCLES, MEM_SPEED_FAST_CYCLES},
    {MEM_SPEED_SLOW_CYCLES, MEM_SPEED_SLOW_CYCLES},
    {MEM_SPEED_FAST_CYCLES, MEM_SPEED_FAST_CYCLES},
    {MEM_SPEED_MED_CYCLES,  MEM_SPEED_MED_CYCLES}
};

uint16_t interrupt_vectors[][2] = {
    [CPU_INT_BRK] = {0xffe6, 0xfffe},
    [CPU_INT_IRQ] = {0xffee, 0xfffe},
    [CPU_INT_NMI] = {0xffea, 0xfffa},
    [CPU_INT_COP] = {0xffe4, 0xfff4},
    [CPU_INT_RES] = {0xfffc, 0xfffc}
};

uint32_t            cpu_cycle_count = 0;
extern uint64_t     master_cycles;
extern uint8_t *    ram1_regs;
extern uint8_t      last_bus_value;
extern uint16_t     vcounter;
extern uint16_t     hcounter;
extern uint32_t     scanline_cycles;

#define ALU_WIDTH_WORD 0
#define ALU_WIDTH_BYTE 1

#define OPCODE(op, cat, addr_mod) {.opcode = op, .address_mode = addr_mod, .opcode_class = cat}

struct inst_t fetch_inst = {
    .uops = {
        MOV_LPC         (MOV_LSB, REG_INST),
        DECODE
    }
};

struct inst_t instructions[] = {

    /**************************************************************************************/
    /*                                      ADC                                           */
    /**************************************************************************************/

    [ADC_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ADC_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ADC_ABS_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ADC_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ADC_ABSL_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ADC_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ADC_S_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        },
    },
    [ADC_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ADC_DIR_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ADC_DIR_INDL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ADC_S_REL_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            IO,
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),

        }
    },

    [ADC_DIR_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ADC_DIR_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPS       (2, STATUS_FLAG_X),
            SKIPC       (1, STATUS_FLAG_PAGE),
            IO          /* X = 0 or page crossed */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ADC_DIR_INDL_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, 0),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ADC_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_M),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            ALU_OP      (ALU_OP_ADD, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      AND                                           */
    /**************************************************************************************/

    [AND_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [AND_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [AND_ABS_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [AND_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [AND_ABSL_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [AND_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [AND_S_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        },
    },
    [AND_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [AND_DIR_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [AND_DIR_INDL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [AND_S_REL_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            IO,
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),

        }
    },

    [AND_DIR_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [AND_DIR_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, 0),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPS       (2, STATUS_FLAG_X),
            SKIPC       (1, STATUS_FLAG_PAGE),
            IO          /* X = 0 or page crossed */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [AND_DIR_INDL_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, 0),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [AND_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_M),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            ALU_OP      (ALU_OP_AND, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      ASL                                           */
    /**************************************************************************************/

    [ASL_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_SHL, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [ASL_ACC] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_SHL, STATUS_FLAG_M, REG_ACCUM, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ASL_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SHL, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [ASL_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_SHL, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
        }
    },

    [ASL_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            IO,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_SHL, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
        }
    },

    /**************************************************************************************/
    /*                                      BCC                                           */
    /**************************************************************************************/

    [BCC_PC_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            SEXT        (REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            SKIPS       (2, STATUS_FLAG_C),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      BCS                                           */
    /**************************************************************************************/

    [BCS_PC_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            SEXT        (REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            SKIPC       (2, STATUS_FLAG_C),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      BEQ                                           */
    /**************************************************************************************/

    [BEQ_PC_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            SEXT        (REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            SKIPC       (2, STATUS_FLAG_Z),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, 0),
        }
    },

    /**************************************************************************************/
    /*                                      BIT                                           */
    /**************************************************************************************/

    [BIT_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_BIT, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
        }
    },

    [BIT_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_BIT, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
        }
    },

    [BIT_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_BIT, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
        }
    },

    [BIT_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_BIT, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
        }
    },

    [BIT_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_M),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            ALU_OP      (ALU_OP_BIT_IMM, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
        }
    },

    /**************************************************************************************/
    /*                                      BMI                                           */
    /**************************************************************************************/

    [BMI_PC_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            SEXT        (REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            SKIPC       (2, STATUS_FLAG_N),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      BNE                                           */
    /**************************************************************************************/

    [BNE_PC_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            SEXT        (REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            SKIPS       (2, STATUS_FLAG_Z),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      BPL                                           */
    /**************************************************************************************/

    [BPL_PC_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            SEXT        (REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            SKIPS       (2, STATUS_FLAG_N),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      BRA                                           */
    /**************************************************************************************/

    [BRA_PC_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            SEXT        (REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      BRK                                           */
    /**************************************************************************************/

    [BRK_S] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            /* this sets the B flag in the status register (when in emulation
            mode) and also loads the appropriate interrupt vector into REG_ADDR */
            BRK,

            /* when in native mode... */
            SKIPS       (2, STATUS_FLAG_E),
            /* the program bank register gets pushed onto the stack */
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_PBR),
            DECS,

            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_P       (REG_P, REG_TEMP),
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_TEMP),
            DECS,

            SET_P       (STATUS_FLAG_I),

            /* when in native mode... */
            SKIPS       (1, STATUS_FLAG_E),
            /* the D flag gets cleared */
            CLR_P       (STATUS_FLAG_D),

            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_PC, MOV_RRW_WORD),
            MOV_RRW     (REG_ZERO, REG_PBR, MOV_RRW_BYTE),
            // MOV_LPC     (MOV_LSB, REG_INST),
            // DECODE
        }
    },

    /**************************************************************************************/
    /*                                      BRL                                           */
    /**************************************************************************************/

    [BRL_PC_RELL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            ADDR_OFFR   (REG_PC, ADDR_OFF_BANK_WRAP),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      BVC                                           */
    /**************************************************************************************/

    [BVC_PC_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            SEXT        (REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            SKIPS       (2, STATUS_FLAG_V),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      BVS                                           */
    /**************************************************************************************/

    [BVS_PC_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            SEXT        (REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            SKIPC       (2, STATUS_FLAG_V),
            IO          /* branch taken */,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      CLC                                           */
    /**************************************************************************************/

    [CLC_IMP] = {
        .uops = {
            IO,
            CLR_P       (STATUS_FLAG_C)
        }
    },

    /**************************************************************************************/
    /*                                      CLD                                           */
    /**************************************************************************************/

    [CLD_IMP] = {
        .uops = {
            IO,
            CLR_P       (STATUS_FLAG_D)
        }
    },

    /**************************************************************************************/
    /*                                      CLI                                           */
    /**************************************************************************************/

    [CLI_IMP] = {
        .uops = {
            IO,
            CLR_P       (STATUS_FLAG_I)
        }
    },

    /**************************************************************************************/
    /*                                      CLV                                           */
    /**************************************************************************************/

    [CLV_IMP] = {
        .uops = {
            IO,
            CLR_P       (STATUS_FLAG_V)
        }
    },

    /**************************************************************************************/
    /*                                      CMP                                           */
    /**************************************************************************************/

    [CMP_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },
    [CMP_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },
    [CMP_ABS_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },
    [CMP_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },
    [CMP_ABSL_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },
    [CMP_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },
    [CMP_S_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        },
    },
    [CMP_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    [CMP_DIR_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    [CMP_DIR_INDL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    [CMP_S_REL_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            IO,
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    [CMP_DIR_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    [CMP_DIR_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPS       (2, STATUS_FLAG_X),
            SKIPC       (1, STATUS_FLAG_PAGE),
            IO          /* X = 0 or page crossed */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    [CMP_DIR_INDL_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, 0),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            // MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    [CMP_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_M),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    /**************************************************************************************/
    /*                                      COP                                           */
    /**************************************************************************************/

    [COP_S] = {
        .uops = {
            COP,
        }
    },

    /**************************************************************************************/
    /*                                      CPX                                           */
    /**************************************************************************************/

    [CPX_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_X, REG_X, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    [CPX_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_X, REG_X, REG_TEMP),
            // CHK_ZN      (REG_TEMP),
        }
    },

    [CPX_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_X),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_X, REG_X, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    /**************************************************************************************/
    /*                                      CPY                                           */
    /**************************************************************************************/

    [CPY_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_X, REG_Y, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    [CPY_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_X, REG_Y, REG_TEMP),
            // CHK_ZN      (REG_TEMP),
        }
    },

    [CPY_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_X),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            ALU_OP      (ALU_OP_CMP, STATUS_FLAG_X, REG_Y, REG_TEMP),
            // CHK_ZNW     (REG_TEMP, 0),
        }
    },

    /**************************************************************************************/
    /*                                      DEC                                           */
    /**************************************************************************************/

    [DEC_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_DEC, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [DEC_ACC] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_DEC, STATUS_FLAG_M, REG_ACCUM, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [DEC_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_DEC, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [DEC_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_DEC, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [DEC_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            IO,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_DEC, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
        }
    },

    /**************************************************************************************/
    /*                                      DEX                                           */
    /**************************************************************************************/

    [DEX_IMP] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_DEC, STATUS_FLAG_X, REG_X, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_X),
            // CHK_ZN      (REG_X),
        }
    },

    /**************************************************************************************/
    /*                                      DEY                                           */
    /**************************************************************************************/

    [DEY_IMP] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_DEC, STATUS_FLAG_X, REG_Y, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_Y),
            // CHK_ZN      (REG_Y),
        }
    },

    /**************************************************************************************/
    /*                                      EOR                                           */
    /**************************************************************************************/

    [EOR_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [EOR_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [EOR_ABS_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [EOR_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [EOR_ABSL_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [EOR_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [EOR_S_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        },
    },
    [EOR_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [EOR_DIR_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [EOR_DIR_INDL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [EOR_S_REL_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            IO,
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),

        }
    },

    [EOR_DIR_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [EOR_DIR_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPS       (2, STATUS_FLAG_X),
            SKIPC       (1, STATUS_FLAG_PAGE),
            IO          /* X = 0 or page crossed */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [EOR_DIR_INDL_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            // MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [EOR_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_M),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            ALU_OP      (ALU_OP_XOR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      INC                                           */
    /**************************************************************************************/

    [INC_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_INC, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [INC_ACC] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_INC, STATUS_FLAG_M, REG_ACCUM, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [INC_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_INC, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [INC_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_INC, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [INC_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            IO,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_INC, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
        }
    },

    /**************************************************************************************/
    /*                                      INX                                           */
    /**************************************************************************************/

    [INX_IMP] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_INC, STATUS_FLAG_X, REG_X, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_X),
            // CHK_ZN      (REG_X),
        }
    },

    /**************************************************************************************/
    /*                                      INY                                           */
    /**************************************************************************************/

    [INY_IMP] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_INC, STATUS_FLAG_X, REG_Y, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_Y),
            // CHK_ZN      (REG_Y),
        }
    },

    /**************************************************************************************/
    /*                                      JML                                           */
    /**************************************************************************************/

    [JML_ABS_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_PBR),
            MOV_RRW     (REG_TEMP, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      JMP                                           */
    /**************************************************************************************/

    [JMP_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    [JMP_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
            MOV_RRW     (REG_BANK, REG_PBR, MOV_RRW_BYTE),
        }
    },

    [JMP_ABS_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW      (REG_TEMP, REG_PC, MOV_RRW_WORD),
        }
    },

    [JMP_ABS_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            IO,
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_PBR, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_PBR, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      JSL                                           */
    /**************************************************************************************/

    [JSL_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_PBR),
            DECS,
            IO,
            MOV_L       (MOV_LSB, REG_PC, REG_PBR, REG_BANK),
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
            MOV_RRW     (REG_BANK, REG_PBR, MOV_RRW_BYTE),
        }
    },

    /**************************************************************************************/
    /*                                      JSR                                           */
    /**************************************************************************************/

    [JSR_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_L       (MOV_MSB, REG_PC, REG_PBR, REG_ADDR),
            IO,
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    [JSR_ABS_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_L       (MOV_MSB, REG_PC, REG_PBR, REG_ADDR),
            IO,
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_PBR, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_PBR, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      LDA                                           */
    /**************************************************************************************/


    [LDA_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },
    [LDA_ABS_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_ABSL_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_S_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        },
    },

    [LDA_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_DIR_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_DIR_INDL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_S_REL_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            IO,
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),

        }
    },

    [LDA_DIR_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_DIR_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, 0),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPS       (2, STATUS_FLAG_X),
            SKIPC       (1, STATUS_FLAG_PAGE),
            IO          /* X = 0 or page crossed */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_DIR_INDL_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, 0),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            // MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    [LDA_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_M),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            CHK_ZN      (REG_ACCUM)
        }
    },

    /**************************************************************************************/
    /*                                      LDX                                           */
    /**************************************************************************************/

    [LDX_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_X),
            CHK_ZN      (REG_X),
        }
    },

    [LDX_ABS_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_X),
            CHK_ZN      (REG_X),
        }
    },

    [LDX_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_X),
            CHK_ZN      (REG_X),
        }
    },

    [LDX_DIR_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_X),
            CHK_ZN      (REG_X),
        }
    },

    [LDX_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_X),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_X),
            CHK_ZN      (REG_X)
        }
    },

    /**************************************************************************************/
    /*                                      LDY                                           */
    /**************************************************************************************/

    [LDY_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_Y),
            CHK_ZN      (REG_Y),
        }
    },

    [LDY_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_Y),
            CHK_ZN      (REG_Y),
        }
    },

    [LDY_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_Y),
            CHK_ZN      (REG_Y),
        }
    },

    [LDY_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_Y),
            CHK_ZN      (REG_Y),
        }
    },

    [LDY_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_X),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_Y),
            CHK_ZN      (REG_Y)
        }
    },

    /**************************************************************************************/
    /*                                      LSR                                           */
    /**************************************************************************************/

    [LSR_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_SHR, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [LSR_ACC] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_SHR, STATUS_FLAG_M, REG_ACCUM, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [LSR_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SHR, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [LSR_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_SHR, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [LSR_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            IO,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_SHR, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
        }
    },

    /**************************************************************************************/
    /*                                      MVN                                           */
    /**************************************************************************************/

    [MVN_BLK] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_L       (MOV_LSB, REG_X, REG_ADDR, REG_TEMP),
            MOV_S       (MOV_LSB, REG_Y, REG_BANK, REG_TEMP),
            IO,
            INC_RW      (REG_X),
            IO,
            INC_RW      (REG_Y),
            DEC_RW      (REG_ACCUM),
            SKIPS       (1, STATUS_FLAG_AM),
            /* we're not done copying stuff yet, so reset PC
            to the start of the instruction */
            INC_PC      (0xfffd)
//            DEC_RW      (REG_PC),
//            DEC_RW      (REG_PC),
//            DEC_RW      (REG_PC),
        }
    },

    /**************************************************************************************/
    /*                                      MVP                                           */
    /**************************************************************************************/
    [MVP_BLK] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_L       (MOV_LSB, REG_X, REG_ADDR, REG_TEMP),
            MOV_S       (MOV_LSB, REG_Y, REG_BANK, REG_TEMP),
            IO,
            DEC_RW      (REG_X),
            IO,
            DEC_RW      (REG_Y),
            DEC_RW      (REG_ACCUM),
            SKIPS       (1, STATUS_FLAG_AM),
            /* we're not done copying stuff yet, so reset PC
            to the start of the instruction */
            INC_PC      (0xfffd)
//            DEC_RW      (REG_PC),
//            DEC_RW      (REG_PC),
//            DEC_RW      (REG_PC),
        }
    },
    /**************************************************************************************/
    /*                                      NOP                                           */
    /**************************************************************************************/

    [NOP_IMP] = {
        .uops = {
            IO
        }
    },

    /**************************************************************************************/
    /*                                      ORA                                           */
    /**************************************************************************************/

    [ORA_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ORA_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ORA_ABS_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ORA_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ORA_ABSL_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ORA_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [ORA_S_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        },
    },
    [ORA_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ORA_DIR_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ORA_DIR_INDL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ORA_S_REL_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            IO,
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),

        }
    },

    [ORA_DIR_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ORA_DIR_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPS       (2, STATUS_FLAG_X),
            SKIPC       (1, STATUS_FLAG_PAGE),
            IO          /* X = 0 or page crossed */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ORA_DIR_INDL_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            // MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ORA_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_M),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            ALU_OP      (ALU_OP_OR, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      PEA                                           */
    /**************************************************************************************/

    [PEA_S] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_TEMP),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_TEMP),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PEI                                           */
    /**************************************************************************************/

    [PEI_S] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_TEMP),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_TEMP),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PER                                           */
    /**************************************************************************************/

    [PER_S] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            ADDR_OFFRS  (REG_PC, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_ADDR),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_ADDR),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PHA                                           */
    /**************************************************************************************/

    [PHA_S] = {
        .uops = {
            SKIPS       (3, STATUS_FLAG_M),
            IO          /* M = 0 */,
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_ACCUM),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_ACCUM),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PHB                                           */
    /**************************************************************************************/

    [PHB_S] = {
        .uops = {
            IO,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_DBR),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PHD                                           */
    /**************************************************************************************/

    [PHD_S] = {
        .uops = {
            IO,
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_D),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_D),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PHK                                           */
    /**************************************************************************************/

    [PHK_S] = {
        .uops = {
            IO,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_PBR),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PHP                                           */
    /**************************************************************************************/

    [PHP_S] = {
        .uops = {
            IO,
            MOV_P       (REG_P, REG_TEMP),
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_TEMP),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PHX                                           */
    /**************************************************************************************/

    [PHX_S] = {
        .uops = {
            SKIPS       (3, STATUS_FLAG_X),
            IO          /* X = 0 */,
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_X),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_X),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PHY                                           */
    /**************************************************************************************/

    [PHY_S] = {
        .uops = {
            SKIPS       (3, STATUS_FLAG_X),
            IO          /* X = 0 */,
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_Y),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_Y),
            DECS,
        }
    },

    /**************************************************************************************/
    /*                                      PLA                                           */
    /**************************************************************************************/

    [PLA_S] = {
        .uops = {
            IO,
            IO,
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            INCS,
            MOV_L       (MOV_MSB, REG_S, REG_ZERO, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      PLB                                           */
    /**************************************************************************************/

    [PLB_S] = {
        .uops = {
            IO,
            IO,
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_DBR),
            CHK_ZN      (REG_DBR)
        }
    },

    /**************************************************************************************/
    /*                                      PLD                                           */
    /**************************************************************************************/

    [PLD_S] = {
        .uops = {
            IO,
            IO,
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_D),
            INCS,
            MOV_L       (MOV_MSB, REG_S, REG_ZERO, REG_D),
            CHK_ZN      (REG_D)
        }
    },

    /**************************************************************************************/
    /*                                      PLP                                           */
    /**************************************************************************************/

    [PLP_S] = {
        .uops = {
            IO,
            IO,
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_TEMP),
            MOV_P       (REG_TEMP, REG_P),
        }
    },

    /**************************************************************************************/
    /*                                      PLX                                           */
    /**************************************************************************************/
    [PLX_S] = {
        .uops = {
            IO,
            IO,
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_X),
            SKIPS       (2, STATUS_FLAG_X),
            INCS,
            MOV_L       (MOV_MSB, REG_S, REG_ZERO, REG_X),
            CHK_ZN      (REG_X)
        }
    },

    /**************************************************************************************/
    /*                                      PLY                                           */
    /**************************************************************************************/

    [PLY_S] = {
        .uops = {
            IO,
            IO,
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_Y),
            SKIPS       (2, STATUS_FLAG_X),
            INCS,
            MOV_L       (MOV_MSB, REG_S, REG_ZERO, REG_Y),
            CHK_ZN      (REG_Y)
        }
    },

    /**************************************************************************************/
    /*                                      REP                                           */
    /**************************************************************************************/

    [REP_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            IO,
            CLR_P       (STATUS_FLAG_LAST)
        }
    },

    /**************************************************************************************/
    /*                                      ROL                                           */
    /**************************************************************************************/

    [ROL_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_ROL, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [ROL_ACC] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_ROL, STATUS_FLAG_M, REG_ACCUM, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ROL_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ROL, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [ROL_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_ROL, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [ROL_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            IO,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_ROL, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
        }
    },

    /**************************************************************************************/
    /*                                      ROR                                           */
    /**************************************************************************************/

    [ROR_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_ROR, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [ROR_ACC] = {
        .uops = {
            IO,
            ALU_OP      (ALU_OP_ROR, STATUS_FLAG_M, REG_ACCUM, REG_ZERO),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [ROR_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_ROR, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [ROR_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_ROR, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
        }
    },

    [ROR_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            IO,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            IO,
            ALU_OP      (ALU_OP_ROR, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            // CHK_ZNW     (REG_TEMP, 0),
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_WRAP, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
        }
    },

    /**************************************************************************************/
    /*                                      RTI                                           */
    /**************************************************************************************/

    [RTI_S] = {
        .uops = {
            IO,
            IO,
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_TEMP),
            MOV_P       (REG_TEMP, REG_P),
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_TEMP),
            INCS,
            MOV_L       (MOV_MSB, REG_S, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_PC, MOV_RRW_WORD),
            SKIPS       (2, STATUS_FLAG_E),
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_PBR),
        }
    },

    /**************************************************************************************/
    /*                                      RTL                                           */
    /**************************************************************************************/

    [RTL_S] = {
        .uops = {
            IO,
            IO,
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_ADDR),
            INCS,
            MOV_L       (MOV_MSB, REG_S, REG_ZERO, REG_ADDR),
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_BANK),
            IO,
            INC_RW      (REG_ADDR),
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
            MOV_RRW     (REG_BANK, REG_PBR, MOV_RRW_BYTE),
        }
    },

    /**************************************************************************************/
    /*                                      RTS                                           */
    /**************************************************************************************/

    [RTS_S] = {
        .uops = {
            IO,
            IO,
            INCS,
            MOV_L       (MOV_LSB, REG_S, REG_ZERO, REG_ADDR),
            INCS,
            MOV_L       (MOV_MSB, REG_S, REG_ZERO, REG_ADDR),
            IO,
            INC_RW      (REG_ADDR),
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      SBC                                           */
    /**************************************************************************************/

    [SBC_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [SBC_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [SBC_ABS_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPC       (2, STATUS_FLAG_PAGE),
            SKIPS       (1, STATUS_FLAG_X),
            IO          /* X=0 or page boundary */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [SBC_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
        }
    },

    [SBC_ABSL_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [SBC_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },
    [SBC_S_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        },
    },
    [SBC_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [SBC_DIR_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, 0),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [SBC_DIR_INDL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [SBC_S_REL_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            IO,
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),

        }
    },

    [SBC_DIR_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [SBC_DIR_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, 0),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPS       (2, STATUS_FLAG_X),
            SKIPC       (1, STATUS_FLAG_PAGE),
            IO          /* X = 0 or page crossed */,
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [SBC_DIR_INDL_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, 0),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            // MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    [SBC_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            SKIPS       (1, STATUS_FLAG_M),
            MOV_LPC     (MOV_MSB, REG_TEMP),
            ALU_OP      (ALU_OP_SUB, STATUS_FLAG_M, REG_ACCUM, REG_TEMP),
            MOV_RR      (REG_TEMP, REG_ACCUM),
            // CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      SEC                                           */
    /**************************************************************************************/

    [SEC_IMP] = {
        .uops = {
            IO,
            SET_P       (STATUS_FLAG_C)
        },
    },

    /**************************************************************************************/
    /*                                      SED                                           */
    /**************************************************************************************/

    [SED_IMP] = {
        .uops = {
            IO,
            SET_P       (STATUS_FLAG_D)
        },
    },

    /**************************************************************************************/
    /*                                      SEI                                           */
    /**************************************************************************************/

    [SEI_IMP] = {
        .uops = {
            IO,
            SET_P       (STATUS_FLAG_I)
        },
    },

    /**************************************************************************************/
    /*                                      SEP                                           */
    /**************************************************************************************/

    [SEP_IMM] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_TEMP),
            IO,
            SET_P       (STATUS_FLAG_LAST)
        }
    },

    /**************************************************************************************/
    /*                                      STA                                           */
    /**************************************************************************************/

    [STA_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },
    [STA_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            IO          /* extra cycle due to write */,
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },
    [STA_ABS_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            IO          /* extra cycle due to write */,
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },
    [STA_ABSL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },
    [STA_ABSL_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_LPC     (MOV_LSB, REG_BANK),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },
    [STA_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },
    [STA_S_REL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_ACCUM),
        },
    },

    [STA_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_ACCUM),
        }
    },

    [STA_DIR_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },

    [STA_DIR_INDL] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },

    [STA_S_REL_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            IO,
            ADDR_OFFR   (REG_S, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            IO,
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },

    [STA_DIR_X_IND] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            IO,
            MOV_L       (MOV_LSB, REG_ADDR, REG_ZERO, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_L       (MOV_MSB, REG_ADDR, REG_ZERO, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },

    [STA_DIR_IND_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            SKIPS       (2, STATUS_FLAG_X),
            SKIPC       (1, STATUS_FLAG_PAGE),
            IO          /* X = 0 or page crossed */,
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },

    [STA_DIR_INDL_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, 0),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_BANK),
            MOV_RRW     (REG_TEMP, REG_ADDR, MOV_RRW_WORD),
            // MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ACCUM),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      STX                                           */
    /**************************************************************************************/

    [STP_IMP] = {
        .uops = {
            IO,
            IO,
            STP,
        }
    },

    /**************************************************************************************/
    /*                                      STX                                           */
    /**************************************************************************************/

    [STX_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_X),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_X),
        }
    },
    [STX_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0  */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_X),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_X),
        }
    },
    [STX_DIR_Y] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_Y, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            IO,
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_X),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_X),
        }
    },

    /**************************************************************************************/
    /*                                      STY                                           */
    /**************************************************************************************/

    [STY_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_Y),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_Y),
        }
    },
    [STY_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL !=0  */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_Y),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_Y),
        }
    },
    [STY_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            IO,
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_Y),
            SKIPS       (2, STATUS_FLAG_X),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_Y),
        }
    },

    /**************************************************************************************/
    /*                                      STZ                                           */
    /**************************************************************************************/

    [STZ_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ZERO),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ZERO),
        }
    },

    [STZ_ABS_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_NEXT),
            IO          /* extra cycle due to write */,
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ZERO),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ZERO),
        }
    },

    [STZ_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL !=0  */,
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_ZERO),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_ZERO),
        }
    },

    [STZ_DIR_X] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            ADDR_OFFR   (REG_X, ADDR_OFF_BANK_WRAP),
            SKIPC       (1, STATUS_FLAG_DL),
            IO          /* DL != 0 */,
            IO,
            MOV_S       (MOV_LSB, REG_ADDR, REG_ZERO, REG_ZERO),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_WRAP),
            MOV_S       (MOV_MSB, REG_ADDR, REG_ZERO, REG_ZERO),
        }
    },

    /**************************************************************************************/
    /*                                      TAX                                           */
    /**************************************************************************************/


    [TAX_IMP] = {
        .uops = {
            IO,
            MOV_RR      (REG_ACCUM, REG_X),
            CHK_ZN      (REG_X),
        }
    },

    /**************************************************************************************/
    /*                                      TAY                                           */
    /**************************************************************************************/

    [TAY_IMP] = {
        .uops = {
            IO,
            MOV_RR      (REG_ACCUM, REG_Y),
            CHK_ZN      (REG_Y),
        }
    },

    /**************************************************************************************/
    /*                                      TCD                                           */
    /**************************************************************************************/

    [TCD_IMP] = {
        .uops = {
            IO,
            MOV_RRW     (REG_ACCUM, REG_D, MOV_RRW_WORD),
            CHK_ZNW     (REG_D, CHK_ZW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      TRB                                           */
    /**************************************************************************************/

    [TRB_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_TRB, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            IO,
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP)
        }
    },

    [TRB_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_TRB, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            IO,
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP)
        }
    },

    /**************************************************************************************/
    /*                                      TSB                                           */
    /**************************************************************************************/

    [TSB_ABS] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_DBR, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_TSB, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            IO,
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP)
        }
    },

    [TSB_DIR] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_ADDR),
            ZEXT        (REG_ADDR),
            ADDR_OFFR   (REG_D, ADDR_OFF_BANK_WRAP),
            MOV_RRW     (REG_ZERO, REG_BANK, MOV_RRW_BYTE),
            MOV_L       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP),
            SKIPS       (2, STATUS_FLAG_M),
            ADDR_OFFI   (1, ADDR_OFF_BANK_NEXT),
            MOV_L       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ALU_OP      (ALU_OP_TSB, STATUS_FLAG_M, REG_TEMP, REG_ZERO),
            IO,
            SKIPS       (2, STATUS_FLAG_M),
            MOV_S       (MOV_MSB, REG_ADDR, REG_BANK, REG_TEMP),
            ADDR_OFFIS  (0xffff, ADDR_OFF_BANK_NEXT, ADDR_OFF_SIGNED),
            MOV_S       (MOV_LSB, REG_ADDR, REG_BANK, REG_TEMP)
        }
    },

    /**************************************************************************************/
    /*                                      TCS                                           */
    /**************************************************************************************/

    [TCS_IMP] = {
        .uops = {
            IO,
            MOV_RRW     (REG_ACCUM, REG_S, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      TDC                                           */
    /**************************************************************************************/

    [TDC_IMP] = {
        .uops = {
            IO,
            MOV_RRW     (REG_D, REG_ACCUM, MOV_RRW_WORD),
            CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      TSC                                           */
    /**************************************************************************************/

    [TSC_ACC] = {
        .uops = {
            IO,
            MOV_RRW     (REG_S, REG_ACCUM, MOV_RRW_WORD),
            CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      TSX                                           */
    /**************************************************************************************/

    [TSX_ACC] = {
        .uops = {
            IO,
            MOV_RR      (REG_S, REG_X),
            CHK_ZN      (REG_X),
        }
    },

    /**************************************************************************************/
    /*                                      TXA                                           */
    /**************************************************************************************/

    [TXA_ACC] = {
        .uops = {
            IO,
            MOV_RR      (REG_X, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      TXS                                           */
    /**************************************************************************************/

    [TXS_ACC] = {
        .uops = {
            IO,
            MOV_RRW     (REG_X, REG_S, MOV_RRW_WORD),
        }
    },

    /**************************************************************************************/
    /*                                      TXY                                           */
    /**************************************************************************************/

    [TXY_ACC] = {
        .uops = {
            IO,
            MOV_RR      (REG_X, REG_Y),
            CHK_ZN      (REG_Y),
        }
    },

    /**************************************************************************************/
    /*                                      TYA                                           */
    /**************************************************************************************/

    [TYA_ACC] = {
        .uops = {
            IO,
            MOV_RR      (REG_Y, REG_ACCUM),
            CHK_ZN      (REG_ACCUM),
        }
    },

    /**************************************************************************************/
    /*                                      TYX                                           */
    /**************************************************************************************/

    [TYX_ACC] = {
        .uops = {
            IO,
            MOV_RR      (REG_Y, REG_X),
            CHK_ZN      (REG_X),
        }
    },

    /**************************************************************************************/
    /*                                      WAI                                           */
    /**************************************************************************************/

    [WAI_ACC] = {
        .uops = {
            IO,
            IO,
            WAI
        }
    },

    /**************************************************************************************/
    /*                                      WDM                                           */
    /**************************************************************************************/

    [WDM_ACC] = {
        .uops = {
            // SKIPC(0, 0)
            // INC_PC(0)
            MOV_LPC(MOV_LSB, REG_ADDR),
        }
    },

    /**************************************************************************************/
    /*                                      XBA                                           */
    /**************************************************************************************/

    [XBA_ACC] = {
        .uops = {
            IO,
            IO,
            XBA,
            CHK_ZNW     (REG_ACCUM, CHK_ZW_BYTE)
        }
    },

    /**************************************************************************************/
    /*                                      XCE                                           */
    /**************************************************************************************/

    [XCE_ACC] = {
        .uops = {
            IO,
            XCE
        }
    },


    [FETCH] = {
        .uops = {
            MOV_LPC     (MOV_LSB, REG_INST),
            DECODE
        }
    },

    [INT_HW] = {
        .uops = {
            IO,
            SKIPS       (1, STATUS_FLAG_E),
            IO,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_PBR),
            DECS,
            MOV_S       (MOV_MSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_PC),
            DECS,
            MOV_P       (REG_P, REG_TEMP),
            MOV_S       (MOV_LSB, REG_S, REG_ZERO, REG_TEMP),
            DECS,
            SET_P       (STATUS_FLAG_I),
            BRK,
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
            MOV_RRW     (REG_ZERO, REG_PBR, MOV_RRW_BYTE),
            MOV_LPC     (MOV_LSB, REG_ADDR),
            MOV_LPC     (MOV_MSB, REG_ADDR),
            MOV_RRW     (REG_ADDR, REG_PC, MOV_RRW_WORD),
        }
    },
};

struct opcode_t opcode_matrix[256] =
{
    [0x61] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_INDIRECT),
    [0x63] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE),
    [0x65] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x67] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG),
    [0x69] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_IMMEDIATE),
    [0x6d] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0x6f] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG),
    [0x71] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_INDEXED),
    [0x72] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT),
    [0x73] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED),
    [0x75] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x77] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED),
    [0x79] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),
    [0x7d] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),
    [0x7f] = OPCODE(OPCODE_ADC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X),

    [0x21] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_INDIRECT),
    [0x23] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE),
    [0x25] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x27] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG),
    [0x29] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_IMMEDIATE),
    [0x2d] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0x2f] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG),
    [0x31] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_INDEXED),
    [0x32] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT),
    [0x33] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED),
    [0x35] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x37] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED),
    [0x39] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),
    [0x3d] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),
    [0x3f] = OPCODE(OPCODE_AND, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X),

    [0x06] = OPCODE(OPCODE_ASL, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x0a] = OPCODE(OPCODE_ASL, OPCODE_CLASS_ALU, ADDRESS_MODE_ACCUMULATOR),
    [0x0e] = OPCODE(OPCODE_ASL, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0x16] = OPCODE(OPCODE_ASL, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x1e] = OPCODE(OPCODE_ASL, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),

    [0x10] = OPCODE(OPCODE_BPL, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE),
    [0x30] = OPCODE(OPCODE_BMI, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE),
    [0x50] = OPCODE(OPCODE_BVC, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE),
    [0x70] = OPCODE(OPCODE_BVS, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE),
    [0x80] = OPCODE(OPCODE_BRA, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE),
    [0x82] = OPCODE(OPCODE_BRL, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE_LONG),
    [0x90] = OPCODE(OPCODE_BCC, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE),
    [0xb0] = OPCODE(OPCODE_BCS, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE),
    [0xd0] = OPCODE(OPCODE_BNE, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE),
    [0xf0] = OPCODE(OPCODE_BEQ, OPCODE_CLASS_BRANCH, ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE),

    [0x24] = OPCODE(OPCODE_BIT, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x2c] = OPCODE(OPCODE_BIT, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0x34] = OPCODE(OPCODE_BIT, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x3c] = OPCODE(OPCODE_BIT, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),
    [0x89] = OPCODE(OPCODE_BIT, OPCODE_CLASS_ALU, ADDRESS_MODE_IMMEDIATE),

    [0x00] = OPCODE(OPCODE_BRK, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_STACK),

    [0x18] = OPCODE(OPCODE_CLC, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMPLIED),
    [0x38] = OPCODE(OPCODE_SEC, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMPLIED),
    [0x58] = OPCODE(OPCODE_CLI, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMPLIED),
    [0x78] = OPCODE(OPCODE_SEI, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMPLIED),
    [0xb8] = OPCODE(OPCODE_CLV, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMPLIED),
    [0xd8] = OPCODE(OPCODE_CLD, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMPLIED),
    [0xf8] = OPCODE(OPCODE_SED, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMPLIED),

    [0xc2] = OPCODE(OPCODE_REP, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMMEDIATE),
    [0xe2] = OPCODE(OPCODE_SEP, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMMEDIATE),

    [0xc1] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_INDIRECT),
    [0xc3] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE),
    [0xc5] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0xc7] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG),
    [0xc9] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_IMMEDIATE),
    [0xcd] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0xcf] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG),
    [0xd1] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_INDEXED),
    [0xd2] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT),
    [0xd3] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED),
    [0xd5] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0xd7] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED),
    [0xd9] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),
    [0xdd] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),
    [0xdf] = OPCODE(OPCODE_CMP, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X),

    [0x02] = OPCODE(OPCODE_COP, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_STACK),

    [0xe0] = OPCODE(OPCODE_CPX, OPCODE_CLASS_ALU, ADDRESS_MODE_IMMEDIATE),
    [0xe4] = OPCODE(OPCODE_CPX, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0xec] = OPCODE(OPCODE_CPX, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),

    [0xc0] = OPCODE(OPCODE_CPY, OPCODE_CLASS_ALU, ADDRESS_MODE_IMMEDIATE),
    [0xc4] = OPCODE(OPCODE_CPY, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0xcc] = OPCODE(OPCODE_CPY, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),

    [0x3a] = OPCODE(OPCODE_DEC, OPCODE_CLASS_ALU, ADDRESS_MODE_ACCUMULATOR),
    [0xc6] = OPCODE(OPCODE_DEC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0xce] = OPCODE(OPCODE_DEC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0xd6] = OPCODE(OPCODE_DEC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0xde] = OPCODE(OPCODE_DEC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),

    [0xca] = OPCODE(OPCODE_DEX, OPCODE_CLASS_ALU, ADDRESS_MODE_IMPLIED),
    [0x88] = OPCODE(OPCODE_DEY, OPCODE_CLASS_ALU, ADDRESS_MODE_IMPLIED),

    [0x41] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_INDIRECT),
    [0x43] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE),
    [0x45] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x47] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG),
    [0x49] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_IMMEDIATE),
    [0x4d] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0x4f] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG),
    [0x51] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_INDEXED),
    [0x52] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT),
    [0x53] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED),
    [0x55] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x57] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED),
    [0x59] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),
    [0x5d] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),
    [0x5f] = OPCODE(OPCODE_EOR, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X),

    [0x1a] = OPCODE(OPCODE_INC, OPCODE_CLASS_ALU, ADDRESS_MODE_ACCUMULATOR),
    [0xe6] = OPCODE(OPCODE_INC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0xee] = OPCODE(OPCODE_INC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0xf6] = OPCODE(OPCODE_INC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0xfe] = OPCODE(OPCODE_INC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),

    [0xe8] = OPCODE(OPCODE_INX, OPCODE_CLASS_ALU, ADDRESS_MODE_IMPLIED),
    [0xc8] = OPCODE(OPCODE_INY, OPCODE_CLASS_ALU, ADDRESS_MODE_IMPLIED),

    [0x4c] = OPCODE(OPCODE_JMP, OPCODE_CLASS_BRANCH, ADDRESS_MODE_ABSOLUTE),
    [0x5c] = OPCODE(OPCODE_JMP, OPCODE_CLASS_BRANCH, ADDRESS_MODE_ABSOLUTE_LONG),
    [0x6c] = OPCODE(OPCODE_JMP, OPCODE_CLASS_BRANCH, ADDRESS_MODE_ABSOLUTE_INDIRECT),
    [0x7c] = OPCODE(OPCODE_JMP, OPCODE_CLASS_BRANCH, ADDRESS_MODE_ABSOLUTE_INDEXED_INDIRECT),
    [0xdc] = OPCODE(OPCODE_JML, OPCODE_CLASS_BRANCH, ADDRESS_MODE_ABSOLUTE_INDIRECT),

    [0x22] = OPCODE(OPCODE_JSL, OPCODE_CLASS_BRANCH, ADDRESS_MODE_ABSOLUTE_LONG),

    [0x20] = OPCODE(OPCODE_JSR, OPCODE_CLASS_BRANCH, ADDRESS_MODE_ABSOLUTE),
    [0xfc] = OPCODE(OPCODE_JSR, OPCODE_CLASS_BRANCH, ADDRESS_MODE_ABSOLUTE_INDEXED_INDIRECT),

    [0xa1] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT_INDEXED_INDIRECT),
    [0xa3] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_STACK_RELATIVE),
    [0xa5] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT),
    [0xa7] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT_INDIRECT_LONG),
    [0xa9] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_IMMEDIATE),
    [0xad] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_ABSOLUTE),
    [0xaf] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_ABSOLUTE_LONG),
    [0xb1] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT_INDIRECT_INDEXED),
    [0xb2] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT_INDIRECT),
    [0xb3] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED),
    [0xb5] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0xb7] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED),
    [0xb9] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),
    [0xbd] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_ABSOLUTE_INDEXED_X),
    [0xbf] = OPCODE(OPCODE_LDA, OPCODE_CLASS_LOAD, ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X),

    [0xa2] = OPCODE(OPCODE_LDX, OPCODE_CLASS_LOAD, ADDRESS_MODE_IMMEDIATE),
    [0xa6] = OPCODE(OPCODE_LDX, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT),
    [0xae] = OPCODE(OPCODE_LDX, OPCODE_CLASS_LOAD, ADDRESS_MODE_ABSOLUTE),
    [0xb6] = OPCODE(OPCODE_LDX, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT_INDEXED_Y),
    [0xbe] = OPCODE(OPCODE_LDX, OPCODE_CLASS_LOAD, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),

    [0xa0] = OPCODE(OPCODE_LDY, OPCODE_CLASS_LOAD, ADDRESS_MODE_IMMEDIATE),
    [0xa4] = OPCODE(OPCODE_LDY, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT),
    [0xac] = OPCODE(OPCODE_LDY, OPCODE_CLASS_LOAD, ADDRESS_MODE_ABSOLUTE),
    [0xb4] = OPCODE(OPCODE_LDY, OPCODE_CLASS_LOAD, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0xbc] = OPCODE(OPCODE_LDY, OPCODE_CLASS_LOAD, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),

    [0x46] = OPCODE(OPCODE_LSR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x4a] = OPCODE(OPCODE_LSR, OPCODE_CLASS_ALU, ADDRESS_MODE_ACCUMULATOR),
    [0x4e] = OPCODE(OPCODE_LSR, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0x56] = OPCODE(OPCODE_LSR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x5e] = OPCODE(OPCODE_LSR, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),

    [0x54] = OPCODE(OPCODE_MVN, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_BLOCK_MOVE),
    [0x44] = OPCODE(OPCODE_MVP, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_BLOCK_MOVE),

    [0xea] = OPCODE(OPCODE_NOP, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMPLIED),

    [0x01] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_INDIRECT),
    [0x03] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE),
    [0x05] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x07] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG),
    [0x09] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_IMMEDIATE),
    [0x0d] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0x0f] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG),
    [0x11] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_INDEXED),
    [0x12] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT),
    [0x13] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED),
    [0x15] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x17] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED),
    [0x19] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),
    [0x1d] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),
    [0x1f] = OPCODE(OPCODE_ORA, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X),


    [0xf4] = OPCODE(OPCODE_PEA, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0xd4] = OPCODE(OPCODE_PEI, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x62] = OPCODE(OPCODE_PER, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x48] = OPCODE(OPCODE_PHA, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x8b] = OPCODE(OPCODE_PHB, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x0b] = OPCODE(OPCODE_PHD, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x4b] = OPCODE(OPCODE_PHK, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x08] = OPCODE(OPCODE_PHP, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0xda] = OPCODE(OPCODE_PHX, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x5a] = OPCODE(OPCODE_PHY, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x68] = OPCODE(OPCODE_PLA, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0xab] = OPCODE(OPCODE_PLB, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x2b] = OPCODE(OPCODE_PLD, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x28] = OPCODE(OPCODE_PLP, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0xfa] = OPCODE(OPCODE_PLX, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),
    [0x7a] = OPCODE(OPCODE_PLY, OPCODE_CLASS_STACK, ADDRESS_MODE_STACK),

    [0x26] = OPCODE(OPCODE_ROL, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x2a] = OPCODE(OPCODE_ROL, OPCODE_CLASS_ALU, ADDRESS_MODE_ACCUMULATOR),
    [0x2e] = OPCODE(OPCODE_ROL, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0x36] = OPCODE(OPCODE_ROL, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x3e] = OPCODE(OPCODE_ROL, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),

    [0x66] = OPCODE(OPCODE_ROR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x6a] = OPCODE(OPCODE_ROR, OPCODE_CLASS_ALU, ADDRESS_MODE_ACCUMULATOR),
    [0x6e] = OPCODE(OPCODE_ROR, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0x76] = OPCODE(OPCODE_ROR, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x7e] = OPCODE(OPCODE_ROR, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),

    [0x40] = OPCODE(OPCODE_RTI, OPCODE_CLASS_BRANCH, ADDRESS_MODE_STACK),
    [0x6b] = OPCODE(OPCODE_RTL, OPCODE_CLASS_BRANCH, ADDRESS_MODE_STACK),
    [0x60] = OPCODE(OPCODE_RTS, OPCODE_CLASS_BRANCH, ADDRESS_MODE_STACK),

    [0xe1] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_INDIRECT),
    [0xe3] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE),
    [0xe5] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0xe7] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG),
    [0xe9] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_IMMEDIATE),
    [0xed] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),
    [0xef] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG),
    [0xf1] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_INDEXED),
    [0xf2] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT),
    [0xf3] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED),
    [0xf5] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0xf7] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED),
    [0xf9] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),
    [0xfd] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_INDEXED_X),
    [0xff] = OPCODE(OPCODE_SBC, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X),

    [0xdb] = OPCODE(OPCODE_STP, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_IMPLIED),

    [0x81] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT_INDEXED_INDIRECT),
    [0x83] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_STACK_RELATIVE),
    [0x85] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT),
    [0x87] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT_INDIRECT_LONG),
    [0x8d] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_ABSOLUTE),
    [0x8f] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_ABSOLUTE_LONG),
    [0x91] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT_INDIRECT_INDEXED),
    [0x92] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT_INDIRECT),
    [0x93] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED),
    [0x95] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x97] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED),
    [0x99] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_ABSOLUTE_INDEXED_Y),
    [0x9d] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_ABSOLUTE_INDEXED_X),
    [0x9f] = OPCODE(OPCODE_STA, OPCODE_CLASS_STORE, ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X),

    [0x86] = OPCODE(OPCODE_STX, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT),
    [0x8e] = OPCODE(OPCODE_STX, OPCODE_CLASS_STORE, ADDRESS_MODE_ABSOLUTE),
    [0x96] = OPCODE(OPCODE_STX, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT_INDEXED_Y),

    [0x84] = OPCODE(OPCODE_STY, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT),
    [0x8c] = OPCODE(OPCODE_STY, OPCODE_CLASS_STORE, ADDRESS_MODE_ABSOLUTE),
    [0x94] = OPCODE(OPCODE_STY, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT_INDEXED_X),

    [0x64] = OPCODE(OPCODE_STZ, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT),
    [0x74] = OPCODE(OPCODE_STZ, OPCODE_CLASS_STORE, ADDRESS_MODE_DIRECT_INDEXED_X),
    [0x9c] = OPCODE(OPCODE_STZ, OPCODE_CLASS_STORE, ADDRESS_MODE_ABSOLUTE),
    [0x9e] = OPCODE(OPCODE_STZ, OPCODE_CLASS_STORE, ADDRESS_MODE_ABSOLUTE_INDEXED_X),

    [0xaa] = OPCODE(OPCODE_TAX, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_IMPLIED),
    [0xa8] = OPCODE(OPCODE_TAY, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_IMPLIED),
    [0x5b] = OPCODE(OPCODE_TCD, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_IMPLIED),
    [0x1b] = OPCODE(OPCODE_TCS, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_IMPLIED),
    [0x7b] = OPCODE(OPCODE_TDC, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_IMPLIED),

    [0x14] = OPCODE(OPCODE_TRB, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x1c] = OPCODE(OPCODE_TRB, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),

    [0x04] = OPCODE(OPCODE_TSB, OPCODE_CLASS_ALU, ADDRESS_MODE_DIRECT),
    [0x0c] = OPCODE(OPCODE_TSB, OPCODE_CLASS_ALU, ADDRESS_MODE_ABSOLUTE),

    [0x3b] = OPCODE(OPCODE_TSC, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_ACCUMULATOR),
    [0xba] = OPCODE(OPCODE_TSX, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_ACCUMULATOR),
    [0x8a] = OPCODE(OPCODE_TXA, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_ACCUMULATOR),
    [0x9a] = OPCODE(OPCODE_TXS, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_ACCUMULATOR),
    [0x9b] = OPCODE(OPCODE_TXY, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_ACCUMULATOR),
    [0x98] = OPCODE(OPCODE_TYA, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_ACCUMULATOR),
    [0xbb] = OPCODE(OPCODE_TYX, OPCODE_CLASS_TRANSFER, ADDRESS_MODE_ACCUMULATOR),
    [0xcb] = OPCODE(OPCODE_WAI, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_ACCUMULATOR),
    [0x42] = OPCODE(OPCODE_WDM, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_ACCUMULATOR),
    [0xeb] = OPCODE(OPCODE_XBA, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_ACCUMULATOR),
    [0xfb] = OPCODE(OPCODE_XCE, OPCODE_CLASS_STANDALONE, ADDRESS_MODE_ACCUMULATOR)
};

struct opcode_info_t opcode_info[] = {
    [ADC_IMM]           = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_M},
    [ADC_ABS]           = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [ADC_ABSL]          = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},
    [ADC_DIR]           = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [ADC_DIR_IND]       = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT,                 .width[0] = 2},
    [ADC_DIR_INDL]      = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG,            .width[0] = 2},
    [ADC_ABS_X]         = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [ADC_ABSL_X]        = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X,         .width[0] = 4},
    [ADC_ABS_Y]         = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_Y,              .width[0] = 3},
    [ADC_DIR_X]         = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},
    [ADC_DIR_X_IND]     = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_INDIRECT,         .width[0] = 2},
    [ADC_DIR_IND_Y]     = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_INDEXED,         .width[0] = 2},
    [ADC_DIR_INDL_Y]    = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED,    .width[0] = 2},
    [ADC_S_REL]         = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_STACK_RELATIVE,                  .width[0] = 2},
    [ADC_S_REL_IND_Y]   = {.opcode = OPCODE_ADC, .addr_mode = ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED, .width[0] = 2},

    [AND_IMM]           = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_M},
    [AND_ABS]           = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [AND_ABSL]          = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},
    [AND_DIR]           = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [AND_DIR_IND]       = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT,                 .width[0] = 2},
    [AND_DIR_INDL]      = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG,            .width[0] = 2},
    [AND_ABS_X]         = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [AND_ABSL_X]        = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X,         .width[0] = 4},
    [AND_ABS_Y]         = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_Y,              .width[0] = 3},
    [AND_DIR_X]         = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},
    [AND_DIR_X_IND]     = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_INDIRECT,         .width[0] = 2},
    [AND_DIR_IND_Y]     = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_INDEXED,         .width[0] = 2},
    [AND_DIR_INDL_Y]    = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED,    .width[0] = 2},
    [AND_S_REL]         = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_STACK_RELATIVE,                  .width[0] = 2},
    [AND_S_REL_IND_Y]   = {.opcode = OPCODE_AND, .addr_mode = ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED, .width[0] = 2},

    [ASL_ACC]           = {.opcode = OPCODE_ASL, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [ASL_ABS]           = {.opcode = OPCODE_ASL, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [ASL_DIR]           = {.opcode = OPCODE_ASL, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [ASL_ABS_X]         = {.opcode = OPCODE_ASL, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},  
    [ASL_DIR_X]         = {.opcode = OPCODE_ASL, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [BCC_PC_REL]        = {.opcode = OPCODE_BCC, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE,        .width[0] = 2},
    [BCS_PC_REL]        = {.opcode = OPCODE_BCS, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE,        .width[0] = 2},
    [BEQ_PC_REL]        = {.opcode = OPCODE_BEQ, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE,        .width[0] = 2},
    [BMI_PC_REL]        = {.opcode = OPCODE_BMI, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE,        .width[0] = 2},
    [BNE_PC_REL]        = {.opcode = OPCODE_BNE, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE,        .width[0] = 2},
    [BPL_PC_REL]        = {.opcode = OPCODE_BPL, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE,        .width[0] = 2},
    [BRA_PC_REL]        = {.opcode = OPCODE_BRA, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE,        .width[0] = 2},
    [BRL_PC_RELL]       = {.opcode = OPCODE_BRL, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE_LONG,   .width[0] = 3},
    [BVC_PC_REL]        = {.opcode = OPCODE_BVC, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE,        .width[0] = 2},
    [BVS_PC_REL]        = {.opcode = OPCODE_BVS, .addr_mode = ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE,        .width[0] = 2},

    [BIT_IMM]           = {.opcode = OPCODE_BIT, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_M},
    [BIT_ABS]           = {.opcode = OPCODE_BIT, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [BIT_DIR]           = {.opcode = OPCODE_BIT, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [BIT_ABS_X]         = {.opcode = OPCODE_BIT, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [BIT_DIR_X]         = {.opcode = OPCODE_BIT, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [BRK_S]             = {.opcode = OPCODE_BRK, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 2, .width[1] = 1, .width_flag = STATUS_FLAG_E},

    [CLC_IMP]           = {.opcode = OPCODE_CLC, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [CLD_IMP]           = {.opcode = OPCODE_CLD, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [CLI_IMP]           = {.opcode = OPCODE_CLI, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [CLV_IMP]           = {.opcode = OPCODE_CLV, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},

    [CMP_IMM]           = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_M},
    [CMP_ABS]           = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [CMP_ABSL]          = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},
    [CMP_DIR]           = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [CMP_DIR_IND]       = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT,                 .width[0] = 2},
    [CMP_DIR_INDL]      = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG,            .width[0] = 2},
    [CMP_ABS_X]         = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [CMP_ABSL_X]        = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X,         .width[0] = 4},
    [CMP_ABS_Y]         = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_Y,              .width[0] = 3},
    [CMP_DIR_X]         = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},
    [CMP_DIR_X_IND]     = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_INDIRECT,         .width[0] = 2},
    [CMP_DIR_IND_Y]     = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_INDEXED,         .width[0] = 2},
    [CMP_DIR_INDL_Y]    = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED,    .width[0] = 2},
    [CMP_S_REL]         = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_STACK_RELATIVE,                  .width[0] = 2},
    [CMP_S_REL_IND_Y]   = {.opcode = OPCODE_CMP, .addr_mode = ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED, .width[0] = 2},

    [COP_S]             = {.opcode = OPCODE_COP, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},

    [CPX_IMM]           = {.opcode = OPCODE_CPX, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_X},
    [CPX_ABS]           = {.opcode = OPCODE_CPX, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [CPX_DIR]           = {.opcode = OPCODE_CPX, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},

    [CPY_IMM]           = {.opcode = OPCODE_CPY, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_X},
    [CPY_ABS]           = {.opcode = OPCODE_CPY, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [CPY_DIR]           = {.opcode = OPCODE_CPY, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},

    [DEC_ACC]           = {.opcode = OPCODE_DEC, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [DEC_ABS]           = {.opcode = OPCODE_DEC, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [DEC_DIR]           = {.opcode = OPCODE_DEC, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [DEC_ABS_X]         = {.opcode = OPCODE_DEC, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [DEC_DIR_X]         = {.opcode = OPCODE_DEC, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [DEX_IMP]           = {.opcode = OPCODE_DEX, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [DEY_IMP]           = {.opcode = OPCODE_DEY, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},

    [EOR_IMM]           = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_M},
    [EOR_ABS]           = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [EOR_ABSL]          = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},
    [EOR_DIR]           = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [EOR_DIR_IND]       = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT,                 .width[0] = 2},
    [EOR_DIR_INDL]      = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG,            .width[0] = 2},
    [EOR_ABS_X]         = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [EOR_ABSL_X]        = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X,         .width[0] = 4},
    [EOR_ABS_Y]         = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_Y,              .width[0] = 3},
    [EOR_DIR_X]         = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},
    [EOR_DIR_X_IND]     = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_INDIRECT,         .width[0] = 2},
    [EOR_DIR_IND_Y]     = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_INDEXED,         .width[0] = 2},
    [EOR_DIR_INDL_Y]    = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED,    .width[0] = 2},
    [EOR_S_REL]         = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_STACK_RELATIVE,                  .width[0] = 2},
    [EOR_S_REL_IND_Y]   = {.opcode = OPCODE_EOR, .addr_mode = ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED, .width[0] = 2},

    [INC_ACC]           = {.opcode = OPCODE_INC, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [INC_ABS]           = {.opcode = OPCODE_INC, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [INC_DIR]           = {.opcode = OPCODE_INC, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [INC_ABS_X]         = {.opcode = OPCODE_INC, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [INC_DIR_X]         = {.opcode = OPCODE_INC, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [INX_IMP]           = {.opcode = OPCODE_INX, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [INY_IMP]           = {.opcode = OPCODE_INY, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},

    [JMP_ABS]           = {.opcode = OPCODE_JMP, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [JMP_ABS_IND]       = {.opcode = OPCODE_JMP, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDIRECT,               .width[0] = 3},
    [JMP_ABS_X_IND]     = {.opcode = OPCODE_JMP, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_INDIRECT,       .width[0] = 3},
    [JMP_ABSL]          = {.opcode = OPCODE_JMP, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},
    [JML_ABS_IND]       = {.opcode = OPCODE_JMP, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDIRECT,               .width[0] = 3},

    [JSR_ABS]           = {.opcode = OPCODE_JSR, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [JSR_ABS_X_IND]     = {.opcode = OPCODE_JSR, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_INDIRECT,       .width[0] = 3},
    [JSL_ABSL]          = {.opcode = OPCODE_JSL, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},

    [LDA_IMM]           = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_M},
    [LDA_ABS]           = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [LDA_ABSL]          = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},
    [LDA_DIR]           = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [LDA_DIR_IND]       = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT,                 .width[0] = 2},
    [LDA_DIR_INDL]      = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG,            .width[0] = 2},
    [LDA_ABS_X]         = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [LDA_ABSL_X]        = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X,         .width[0] = 4},
    [LDA_ABS_Y]         = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_Y,              .width[0] = 3},
    [LDA_DIR_X]         = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},
    [LDA_DIR_X_IND]     = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_INDIRECT,         .width[0] = 2},
    [LDA_DIR_IND_Y]     = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_INDEXED,         .width[0] = 2},
    [LDA_DIR_INDL_Y]    = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED,    .width[0] = 2},
    [LDA_S_REL]         = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_STACK_RELATIVE,                  .width[0] = 2},
    [LDA_S_REL_IND_Y]   = {.opcode = OPCODE_LDA, .addr_mode = ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED, .width[0] = 2},

    [LDX_IMM]           = {.opcode = OPCODE_LDX, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_X},
    [LDX_ABS]           = {.opcode = OPCODE_LDX, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [LDX_DIR]           = {.opcode = OPCODE_LDX, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [LDX_ABS_Y]         = {.opcode = OPCODE_LDX, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_Y,              .width[0] = 3},
    [LDX_DIR_Y]         = {.opcode = OPCODE_LDX, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_Y,                .width[0] = 2},

    [LDY_IMM]           = {.opcode = OPCODE_LDY, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_X},
    [LDY_ABS]           = {.opcode = OPCODE_LDY, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [LDY_DIR]           = {.opcode = OPCODE_LDY, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [LDY_ABS_X]         = {.opcode = OPCODE_LDY, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [LDY_DIR_X]         = {.opcode = OPCODE_LDY, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [LSR_ACC]           = {.opcode = OPCODE_LSR, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [LSR_ABS]           = {.opcode = OPCODE_LSR, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [LSR_DIR]           = {.opcode = OPCODE_LSR, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [LSR_ABS_X]         = {.opcode = OPCODE_LSR, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},  
    [LSR_DIR_X]         = {.opcode = OPCODE_LSR, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [MVN_BLK]           = {.opcode = OPCODE_MVN, .addr_mode = ADDRESS_MODE_BLOCK_MOVE,                      .width[0] = 3},
    [MVP_BLK]           = {.opcode = OPCODE_MVP, .addr_mode = ADDRESS_MODE_BLOCK_MOVE,                      .width[0] = 3},

    [NOP_IMP]           = {.opcode = OPCODE_NOP, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},

    [ORA_IMM]           = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_M},
    [ORA_ABS]           = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [ORA_ABSL]          = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},
    [ORA_DIR]           = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [ORA_DIR_IND]       = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT,                 .width[0] = 2},
    [ORA_DIR_INDL]      = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG,            .width[0] = 2},
    [ORA_ABS_X]         = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [ORA_ABSL_X]        = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X,         .width[0] = 4},
    [ORA_ABS_Y]         = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_Y,              .width[0] = 3},
    [ORA_DIR_X]         = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},
    [ORA_DIR_X_IND]     = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_INDIRECT,         .width[0] = 2},
    [ORA_DIR_IND_Y]     = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_INDEXED,         .width[0] = 2},
    [ORA_DIR_INDL_Y]    = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED,    .width[0] = 2},
    [ORA_S_REL]         = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_STACK_RELATIVE,                  .width[0] = 2},
    [ORA_S_REL_IND_Y]   = {.opcode = OPCODE_ORA, .addr_mode = ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED, .width[0] = 2},
    
    [PEA_S]             = {.opcode = OPCODE_PEA, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 3},
    [PEI_S]             = {.opcode = OPCODE_PEI, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 2},
    [PER_S]             = {.opcode = OPCODE_PER, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 3},
    [PHA_S]             = {.opcode = OPCODE_PHA, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PHB_S]             = {.opcode = OPCODE_PHB, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PHD_S]             = {.opcode = OPCODE_PHD, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PHK_S]             = {.opcode = OPCODE_PHK, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PHP_S]             = {.opcode = OPCODE_PHP, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PHX_S]             = {.opcode = OPCODE_PHX, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PHY_S]             = {.opcode = OPCODE_PHY, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},

    [PLA_S]             = {.opcode = OPCODE_PLA, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PLB_S]             = {.opcode = OPCODE_PLB, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PLD_S]             = {.opcode = OPCODE_PLD, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PLP_S]             = {.opcode = OPCODE_PLP, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PLX_S]             = {.opcode = OPCODE_PLX, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [PLY_S]             = {.opcode = OPCODE_PLY, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},

    [REP_IMM]           = {.opcode = OPCODE_REP, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 2},

    [ROL_ACC]           = {.opcode = OPCODE_ROL, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [ROL_ABS]           = {.opcode = OPCODE_ROL, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [ROL_DIR]           = {.opcode = OPCODE_ROL, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [ROL_ABS_X]         = {.opcode = OPCODE_ROL, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [ROL_DIR_X]         = {.opcode = OPCODE_ROL, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [ROR_ACC]           = {.opcode = OPCODE_ROR, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [ROR_ABS]           = {.opcode = OPCODE_ROR, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [ROR_DIR]           = {.opcode = OPCODE_ROR, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [ROR_ABS_X]         = {.opcode = OPCODE_ROR, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [ROR_DIR_X]         = {.opcode = OPCODE_ROR, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [RTI_S]             = {.opcode = OPCODE_RTI, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [RTL_S]             = {.opcode = OPCODE_RTL, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},
    [RTS_S]             = {.opcode = OPCODE_RTS, .addr_mode = ADDRESS_MODE_STACK,                           .width[0] = 1},

    [SBC_IMM]           = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_IMMEDIATE,                       .width[0] = 3, .width[1] = 2, .width_flag = STATUS_FLAG_M},
    [SBC_ABS]           = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [SBC_ABSL]          = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},
    [SBC_DIR]           = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [SBC_DIR_IND]       = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT,                 .width[0] = 2},
    [SBC_DIR_INDL]      = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG,            .width[0] = 2},
    [SBC_ABS_X]         = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [SBC_ABSL_X]        = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X,         .width[0] = 4},
    [SBC_ABS_Y]         = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_Y,              .width[0] = 3},
    [SBC_DIR_X]         = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},
    [SBC_DIR_X_IND]     = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_INDIRECT,         .width[0] = 2},
    [SBC_DIR_IND_Y]     = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_INDEXED,         .width[0] = 2},
    [SBC_DIR_INDL_Y]    = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED,    .width[0] = 2},
    [SBC_S_REL]         = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_STACK_RELATIVE,                  .width[0] = 2},
    [SBC_S_REL_IND_Y]   = {.opcode = OPCODE_SBC, .addr_mode = ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED, .width[0] = 2},

    [SEC_IMP]           = {.opcode = OPCODE_SEC, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [SED_IMP]           = {.opcode = OPCODE_SED, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [SEI_IMP]           = {.opcode = OPCODE_SEI, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [SEP_IMM]           = {.opcode = OPCODE_SEP, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 2},

    [STA_ABS]           = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [STA_ABSL]          = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG,                   .width[0] = 4},
    [STA_DIR]           = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [STA_DIR_IND]       = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT,                 .width[0] = 2},
    [STA_DIR_INDL]      = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG,            .width[0] = 2},
    [STA_ABS_X]         = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [STA_ABSL_X]        = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X,         .width[0] = 4},
    [STA_ABS_Y]         = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_Y,              .width[0] = 3},
    [STA_DIR_X]         = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},
    [STA_DIR_X_IND]     = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_INDIRECT,         .width[0] = 2},
    [STA_DIR_IND_Y]     = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_INDEXED,         .width[0] = 2},
    [STA_DIR_INDL_Y]    = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED,    .width[0] = 2},
    [STA_S_REL]         = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_STACK_RELATIVE,                  .width[0] = 2},
    [STA_S_REL_IND_Y]   = {.opcode = OPCODE_STA, .addr_mode = ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED, .width[0] = 2},

    [STP_IMP]           = {.opcode = OPCODE_STP, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},

    [STX_ABS]           = {.opcode = OPCODE_STX, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [STX_DIR]           = {.opcode = OPCODE_STX, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [STX_DIR_Y]         = {.opcode = OPCODE_STX, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_Y,                .width[0] = 2},

    [STY_ABS]           = {.opcode = OPCODE_STY, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [STY_DIR]           = {.opcode = OPCODE_STY, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [STY_DIR_X]         = {.opcode = OPCODE_STY, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [STZ_ABS]           = {.opcode = OPCODE_STZ, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [STZ_DIR]           = {.opcode = OPCODE_STZ, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},
    [STZ_ABS_X]         = {.opcode = OPCODE_STZ, .addr_mode = ADDRESS_MODE_ABSOLUTE_INDEXED_X,              .width[0] = 3},
    [STZ_DIR_X]         = {.opcode = OPCODE_STZ, .addr_mode = ADDRESS_MODE_DIRECT_INDEXED_X,                .width[0] = 2},

    [TAX_IMP]           = {.opcode = OPCODE_TAX, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [TAY_IMP]           = {.opcode = OPCODE_TAY, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [TCD_IMP]           = {.opcode = OPCODE_TCD, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [TCS_IMP]           = {.opcode = OPCODE_TCS, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [TDC_IMP]           = {.opcode = OPCODE_TDC, .addr_mode = ADDRESS_MODE_IMPLIED,                         .width[0] = 1},
    [TSC_ACC]           = {.opcode = OPCODE_TSC, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [TSX_ACC]           = {.opcode = OPCODE_TSX, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [TXA_ACC]           = {.opcode = OPCODE_TXA, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [TXS_ACC]           = {.opcode = OPCODE_TXS, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [TXY_ACC]           = {.opcode = OPCODE_TXY, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [TYA_ACC]           = {.opcode = OPCODE_TYA, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [TYX_ACC]           = {.opcode = OPCODE_TYX, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},

    [TRB_ABS]           = {.opcode = OPCODE_TRB, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [TRB_DIR]           = {.opcode = OPCODE_TRB, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},

    [TSB_ABS]           = {.opcode = OPCODE_TSB, .addr_mode = ADDRESS_MODE_ABSOLUTE,                        .width[0] = 3},
    [TSB_DIR]           = {.opcode = OPCODE_TSB, .addr_mode = ADDRESS_MODE_DIRECT,                          .width[0] = 2},

    [WAI_ACC]           = {.opcode = OPCODE_WAI, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},

    [WDM_ACC]           = {.opcode = OPCODE_WDM, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 2},

    [XBA_ACC]           = {.opcode = OPCODE_XBA, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
    [XCE_ACC]           = {.opcode = OPCODE_XCE, .addr_mode = ADDRESS_MODE_ACCUMULATOR,                     .width[0] = 1},
};

const char *opcode_strs[] = {
    [OPCODE_BRK] = "BRK",
    [OPCODE_BIT] = "BIT",
    [OPCODE_BCC] = "BCC",
    [OPCODE_BCS] = "BCS",
    [OPCODE_BNE] = "BNE",
    [OPCODE_BEQ] = "BEQ",
    [OPCODE_BPL] = "BPL",
    [OPCODE_BMI] = "BMI",
    [OPCODE_BVC] = "BVC",
    [OPCODE_BVS] = "BVS",
    [OPCODE_BRA] = "BRA",
    [OPCODE_BRL] = "BRL",
    [OPCODE_CLC] = "CLC",
    [OPCODE_CLD] = "CLD",
    [OPCODE_CLV] = "CLV",
    [OPCODE_CLI] = "CLI",
    [OPCODE_CMP] = "CMP",
    [OPCODE_CPX] = "CPX",
    [OPCODE_CPY] = "CPY",
    [OPCODE_ADC] = "ADC",
    [OPCODE_AND] = "AND",
    [OPCODE_SBC] = "SBC",
    [OPCODE_EOR] = "EOR",
    [OPCODE_ORA] = "ORA",
    [OPCODE_ROL] = "ROL",
    [OPCODE_ROR] = "ROR",
    [OPCODE_DEC] = "DEC",
    [OPCODE_INC] = "INC",
    [OPCODE_ASL] = "ASL",
    [OPCODE_LSR] = "LSR",
    [OPCODE_DEX] = "DEX",
    [OPCODE_INX] = "INX",
    [OPCODE_DEY] = "DEY",
    [OPCODE_INY] = "INY",
    [OPCODE_TRB] = "TRB",
    [OPCODE_TSB] = "TSB",
    [OPCODE_JMP] = "JMP",
    [OPCODE_JML] = "JML",
    [OPCODE_JSL] = "JSL",
    [OPCODE_JSR] = "JSR",
    [OPCODE_LDA] = "LDA",
    [OPCODE_LDX] = "LDX",
    [OPCODE_LDY] = "LDY",
    [OPCODE_STA] = "STA",
    [OPCODE_STX] = "STX",
    [OPCODE_STY] = "STY",
    [OPCODE_STZ] = "STZ",
    [OPCODE_MVN] = "MVN",
    [OPCODE_MVP] = "MVP",
    [OPCODE_NOP] = "NOP",
    [OPCODE_PEA] = "PEA",
    [OPCODE_PEI] = "PEI",
    [OPCODE_PER] = "PER",
    [OPCODE_PHA] = "PHA",
    [OPCODE_PHB] = "PHB",
    [OPCODE_PHD] = "PHD",
    [OPCODE_PHK] = "PHK",
    [OPCODE_PHP] = "PHP",
    [OPCODE_PHX] = "PHX",
    [OPCODE_PHY] = "PHY",
    [OPCODE_PLA] = "PLA",
    [OPCODE_PLB] = "PLB",
    [OPCODE_PLD] = "PLD",
    [OPCODE_PLP] = "PLP",
    [OPCODE_PLX] = "PLX",
    [OPCODE_PLY] = "PLY",
    [OPCODE_REP] = "REP",
    [OPCODE_RTI] = "RTI",
    [OPCODE_RTL] = "RTL",
    [OPCODE_RTS] = "RTS",
    [OPCODE_SEP] = "SEP",
    [OPCODE_SEC] = "SEC",
    [OPCODE_SED] = "SED",
    [OPCODE_SEI] = "SEI",
    [OPCODE_TAX] = "TAX",
    [OPCODE_TAY] = "TAY",
    [OPCODE_TCD] = "TCD",
    [OPCODE_TCS] = "TCS",
    [OPCODE_TDC] = "TDC",
    [OPCODE_TSC] = "TSC",
    [OPCODE_TSX] = "TSX",
    [OPCODE_TXA] = "TXA",
    [OPCODE_TXS] = "TXS",
    [OPCODE_TXY] = "TXY",
    [OPCODE_TYA] = "TYA",
    [OPCODE_TYX] = "TYX",
    [OPCODE_WAI] = "WAI",
    [OPCODE_WDM] = "WDM",
    [OPCODE_XBA] = "XBA",
    [OPCODE_STP] = "STP",
    [OPCODE_COP] = "COP",
    [OPCODE_XCE] = "XCE",
    [OPCODE_UNKNOWN] = "UNKNOWN",
};

const char *addr_mode_strs[] = {
    [ADDRESS_MODE_ABSOLUTE]                         = "a",    
    [ADDRESS_MODE_ABSOLUTE_INDEXED_INDIRECT]        = "(a,x)",
    [ADDRESS_MODE_ABSOLUTE_INDEXED_X]               = "a,x",  
    [ADDRESS_MODE_ABSOLUTE_INDEXED_Y]               = "a,y",  
    [ADDRESS_MODE_ABSOLUTE_INDIRECT]                = "(a)",  
    [ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X]          = "al, x",
    [ADDRESS_MODE_ABSOLUTE_LONG]                    = "al",   
    [ADDRESS_MODE_ACCUMULATOR]                      = "ACC",  
    [ADDRESS_MODE_BLOCK_MOVE]                       = "xyc",  
    [ADDRESS_MODE_DIRECT_INDEXED_INDIRECT]          = "(d, x)",
    [ADDRESS_MODE_DIRECT_INDEXED_X]                 = "d,x",  
    [ADDRESS_MODE_DIRECT_INDEXED_Y]                 = "d,y",  
    [ADDRESS_MODE_DIRECT_INDIRECT_INDEXED]          = "(d),y",
    [ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED]     = "[d],y",
    [ADDRESS_MODE_DIRECT_INDIRECT_LONG]             = "[d]",  
    [ADDRESS_MODE_DIRECT_INDIRECT]                  = "(d)",  
    [ADDRESS_MODE_DIRECT]                           = "d",    
    [ADDRESS_MODE_IMMEDIATE]                        = "#",    
    [ADDRESS_MODE_IMPLIED]                          = "",     
    [ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE_LONG]    = "rl",   
    [ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE]         = "r",    
    [ADDRESS_MODE_STACK]                            = "s",    
    [ADDRESS_MODE_STACK_RELATIVE]                   = "d,s",  
    [ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED]  = "(d,x),y"
};

// const char *cpu_reg_strs[] = {
//     // [CPU_REG_]
// };

// struct branch_cond_t branch_conds[5] =
// {
//     [0] = {CPU_STATUS_FLAG_CARRY},
//     [1] = {CPU_STATUS_FLAG_ZERO},
//     [2] = {CPU_STATUS_FLAG_NEGATIVE},
//     [3] = {CPU_STATUS_FLAG_OVERFLOW},
//     [4] = {0xff}
// };

//struct transfer_params_t transfer_params[] =
//{
//    [TPARM_INDEX(OPCODE_TAX)] = {.src_reg = &cpu_state.reg_accum.reg_accumC, .dst_reg = &cpu_state.reg_x.reg_x,          .flag = CPU_STATUS_FLAG_INDEX},
//    [TPARM_INDEX(OPCODE_TAY)] = {.src_reg = &cpu_state.reg_accum.reg_accumC, .dst_reg = &cpu_state.reg_y.reg_y,          .flag = CPU_STATUS_FLAG_INDEX},
//    [TPARM_INDEX(OPCODE_TCD)] = {.src_reg = &cpu_state.reg_accum.reg_accumC, .dst_reg = &cpu_state.reg_d},
//    [TPARM_INDEX(OPCODE_TCS)] = {.src_reg = &cpu_state.reg_accum.reg_accumC, .dst_reg = &cpu_state.reg_s},
//    [TPARM_INDEX(OPCODE_TDC)] = {.src_reg = &cpu_state.reg_d,                .dst_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [TPARM_INDEX(OPCODE_TSC)] = {.src_reg = &cpu_state.reg_s,                .dst_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [TPARM_INDEX(OPCODE_TSX)] = {.src_reg = &cpu_state.reg_s,                .dst_reg = &cpu_state.reg_x.reg_x,          .flag = CPU_STATUS_FLAG_INDEX},
//    [TPARM_INDEX(OPCODE_TXA)] = {.src_reg = &cpu_state.reg_x.reg_x,          .dst_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [TPARM_INDEX(OPCODE_TXS)] = {.src_reg = &cpu_state.reg_x.reg_x,          .dst_reg = &cpu_state.reg_s},
//    [TPARM_INDEX(OPCODE_TXY)] = {.src_reg = &cpu_state.reg_x.reg_x,          .dst_reg = &cpu_state.reg_y.reg_y,          .flag = CPU_STATUS_FLAG_INDEX},
//    [TPARM_INDEX(OPCODE_TYA)] = {.src_reg = &cpu_state.reg_y.reg_y,          .dst_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [TPARM_INDEX(OPCODE_TYX)] = {.src_reg = &cpu_state.reg_y.reg_y,          .dst_reg = &cpu_state.reg_x.reg_x,          .flag = CPU_STATUS_FLAG_INDEX},
//};

//struct transfer_t transfers[] = {
//    [OPCODE_TAX - OPCODE_TAX] = {.src_reg = &cpu_state.reg_accum.reg_accumC, .dst_reg = &cpu_state.reg_x.reg_x,          .flag = CPU_STATUS_FLAG_INDEX},
//    [OPCODE_TAY - OPCODE_TAX] = {.src_reg = &cpu_state.reg_accum.reg_accumC, .dst_reg = &cpu_state.reg_y.reg_y,          .flag = CPU_STATUS_FLAG_INDEX},
//    [OPCODE_TCD - OPCODE_TAX] = {.src_reg = &cpu_state.reg_accum.reg_accumC, .dst_reg = &cpu_state.reg_d},
//    [OPCODE_TCS - OPCODE_TAX] = {.src_reg = &cpu_state.reg_accum.reg_accumC, .dst_reg = &cpu_state.reg_s},
//    [OPCODE_TDC - OPCODE_TAX] = {.src_reg = &cpu_state.reg_d,                .dst_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [OPCODE_TSC - OPCODE_TAX] = {.src_reg = &cpu_state.reg_s,                .dst_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [OPCODE_TSX - OPCODE_TAX] = {.src_reg = &cpu_state.reg_s,                .dst_reg = &cpu_state.reg_x.reg_x,          .flag = CPU_STATUS_FLAG_INDEX},
//    [OPCODE_TXA - OPCODE_TAX] = {.src_reg = &cpu_state.reg_x.reg_x,          .dst_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [OPCODE_TXS - OPCODE_TAX] = {.src_reg = &cpu_state.reg_x.reg_x,          .dst_reg = &cpu_state.reg_s},
//    [OPCODE_TXY - OPCODE_TAX] = {.src_reg = &cpu_state.reg_x.reg_x,          .dst_reg = &cpu_state.reg_y.reg_y,          .flag = CPU_STATUS_FLAG_INDEX},
//    [OPCODE_TYA - OPCODE_TAX] = {.src_reg = &cpu_state.reg_y.reg_y,          .dst_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [OPCODE_TYX - OPCODE_TAX] = {.src_reg = &cpu_state.reg_y.reg_y,          .dst_reg = &cpu_state.reg_x.reg_x,          .flag = CPU_STATUS_FLAG_INDEX},
//};

// uint32_t zero_reg = 0;

//struct store_t stores[] = {
//    [OPCODE_STA - OPCODE_STA] = {.src_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [OPCODE_STX - OPCODE_STA] = {.src_reg = &cpu_state.reg_x.reg_x,          .flag = CPU_STATUS_FLAG_INDEX},
//    [OPCODE_STY - OPCODE_STA] = {.src_reg = &cpu_state.reg_y.reg_y,          .flag = CPU_STATUS_FLAG_INDEX},
//    [OPCODE_STZ - OPCODE_STA] = {.src_reg = &zero_reg,                       .flag = CPU_STATUS_FLAG_MEMORY}
//};
//
//struct load_t loads[] = {
//    [OPCODE_LDA - OPCODE_LDA] = {.dst_reg = &cpu_state.reg_accum.reg_accumC, .flag = CPU_STATUS_FLAG_MEMORY},
//    [OPCODE_LDX - OPCODE_LDA] = {.dst_reg = &cpu_state.reg_x.reg_x,          .flag = CPU_STATUS_FLAG_INDEX},
//    [OPCODE_LDY - OPCODE_LDA] = {.dst_reg = &cpu_state.reg_y.reg_y,          .flag = CPU_STATUS_FLAG_INDEX},
//};

//  uint8_t carry_flag_alu_op[ALU_OP_LAST] =
//  {
//      [ALU_OP_ADD] = 1,
//      [ALU_OP_SUB] = 1,
//      [ALU_OP_SHL] = 1,
//      [ALU_OP_SHR] = 1,
//      [ALU_OP_ROR] = 1,
//      [ALU_OP_ROL] = 1,
//      [ALU_OP_CMP] = 1
//  };

//struct push_t pushes[] = {
//    [OPCODE_PEA - OPCODE_PEA] = {NULL},
//    [OPCODE_PEI - OPCODE_PEA] = {NULL},
//    [OPCODE_PER - OPCODE_PEA] = {NULL},
//    [OPCODE_PHA - OPCODE_PEA] = {.src_reg = &cpu_state.reg_accum.reg_accumC,    .flag = CPU_STATUS_FLAG_MEMORY},
//    [OPCODE_PHB - OPCODE_PEA] = {.src_reg = &cpu_state.reg_dbrw.reg_dbr},
//    [OPCODE_PHD - OPCODE_PEA] = {.src_reg = &cpu_state.reg_d,                   .flag = 0xffff},
//    [OPCODE_PHK - OPCODE_PEA] = {.src_reg = &cpu_state.reg_pbrw.reg_pbr},
//    [OPCODE_PHP - OPCODE_PEA] = {.src_reg = cpu_state.reg_p},
//    [OPCODE_PHX - OPCODE_PEA] = {.src_reg = &cpu_state.reg_x.reg_x,             .flag = CPU_STATUS_FLAG_INDEX},
//    [OPCODE_PHY - OPCODE_PEA] = {.src_reg = &cpu_state.reg_y.reg_y,             .flag = CPU_STATUS_FLAG_INDEX},
//};

//struct pop_t pops[] = {
//    [OPCODE_PLA - OPCODE_PLA] = {.dst_reg = &cpu_state.reg_accum.reg_accumC,    .flag = CPU_STATUS_FLAG_MEMORY},
//    [OPCODE_PLB - OPCODE_PLA] = {.dst_reg = &cpu_state.reg_dbrw.reg_dbr},
//    [OPCODE_PLD - OPCODE_PLA] = {.dst_reg = &cpu_state.reg_d,                   .flag = 0xffff},
//    [OPCODE_PLP - OPCODE_PLA] = {.dst_reg = &cpu_state.reg_p},
//    [OPCODE_PLX - OPCODE_PLA] = {.dst_reg = &cpu_state.reg_x.reg_x,             .flag = CPU_STATUS_FLAG_INDEX},
//    [OPCODE_PLY - OPCODE_PLA] = {.dst_reg = &cpu_state.reg_y.reg_y,             .flag = CPU_STATUS_FLAG_INDEX},
//};


char *memory_str(unsigned int effective_address)
{
    switch(effective_address)
    {
        case 0x2100:
            return "INIDISP";
        break;

        case 0x2101:
            return "OBJSEL";
        break;

        case 0x2102:
            return "OAMADD";
        break;

        case 0x2104:
            return "OAM DATA";
        break;

        case 0x2105:
            return "BG MODE";
        break;

        case 0x2106:
            return "MOSAIC";
        break;

        case 0x2107:
            return "BG1SC";
        break;

        case 0x2108:
            return "BG2SC";
        break;

        case 0x2109:
            return "BG3SC";
        break;

        case 0x210a:
            return "BG4SC";
        break;

        case 0x210b:
            return "BG12NBA";
        break;

        case 0x210c:
            return "BG34NBA";
        break;



        case 0x210d:
            return "BG1H0FS";
        break;

        case 0x210e:
            return "BG1V0FS";
        break;

        case 0x210f:
            return "BG2H0FS";
        break;

        case 0x2110:
            return "BG2V0FS";
        break;

        case 0x2111:
            return "BG3H0FS";
        break;

        case 0x2112:
            return "BG3V0FS";
        break;

        case 0x2113:
            return "BG4H0FS";
        break;

        case 0x2114:
            return "BG4V0FS";
        break;



        case 0x2115:
            return "VMAINC";
        break;

        case 0x2116:
            return "VMADDL";
        break;

        case 0x2117:
            return "VMADDH";
        break;

        case 0x2118:
            return "VMDATAL";
        break;

        case 0x2119:
            return "VMDATAH";
        break;

        case 0x211a:
            return "M7SEL";
        break;



        case 0x211b:
            return "M7A";
        break;

        case 0x211c:
            return "M7B";
        break;

        case 0x211d:
            return "M7C";
        break;

        case 0x211e:
            return "M7D";
        break;

        case 0x211f:
            return "M7X";
        break;

        case 0x2120:
            return "M7Y";
        break;


        case 0x2121:
            return "CGADD";
        break;

        case 0x2122:
            return "CGDATA";
        break;

        case 0x2123:
            return "W12SEL";
        break;

        case 0x2124:
            return "W34SEL";
        break;

        case 0x2125:
            return "WOBJSEL";
        break;


        case 0x2126:
            return "WH0";
        break;

        case 0x2127:
            return "WH1";
        break;

        case 0x2128:
            return "WH2";
        break;

        case 0x2129:
            return "WH3";
        break;


        case 0x4200:
            return "NMITIMEN";
        break;

        case 0x4201:
            return "WRIO";
        break;

        case 0x4202:
            return "WRMPYA";
        break;

        case 0x4203:
            return "WRMPYB";
        break;

        case 0x420d:
            return "MEMSEL";
        break;

        case 0x4212:
            return "HVBJOY";
        break;

        default:
            return NULL;
        break;
    }
}


uint32_t opcode_width(struct opcode_t *opcode)
{
    uint32_t width = 0;
    switch(opcode->address_mode)
    {
        case ADDRESS_MODE_ABSOLUTE:
        case ADDRESS_MODE_ABSOLUTE_INDEXED_INDIRECT:
        case ADDRESS_MODE_ABSOLUTE_INDEXED_X:
        case ADDRESS_MODE_ABSOLUTE_INDEXED_Y:
        case ADDRESS_MODE_BLOCK_MOVE:
        case ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE_LONG:
            width = 3;
        break;

        case ADDRESS_MODE_ABSOLUTE_INDIRECT:
            if(opcode->opcode == OPCODE_JML)
            {
                width = 4;
            }
            else
            {
                width = 3;
            }
        break;

        case ADDRESS_MODE_ABSOLUTE_LONG:
            width = 4;
        break;

        case ADDRESS_MODE_ACCUMULATOR:
        case ADDRESS_MODE_IMPLIED:
        case ADDRESS_MODE_STACK:
            width = 1;
        break;

        case ADDRESS_MODE_DIRECT_INDEXED_INDIRECT:
        case ADDRESS_MODE_DIRECT_INDEXED_X:
        case ADDRESS_MODE_DIRECT_INDEXED_Y:
        case ADDRESS_MODE_DIRECT_INDIRECT_INDEXED:
        case ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED:
        case ADDRESS_MODE_DIRECT_INDIRECT_LONG:
        case ADDRESS_MODE_DIRECT_INDIRECT:
        case ADDRESS_MODE_DIRECT:
        case ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE:
        case ADDRESS_MODE_STACK_RELATIVE:
        case ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED:
            width = 2;
        break;

        case ADDRESS_MODE_IMMEDIATE:
            switch(opcode->opcode)
            {
                case OPCODE_LDA:
                case OPCODE_CMP:
                case OPCODE_AND:
                case OPCODE_EOR:
                case OPCODE_ORA:
                case OPCODE_ADC:
                case OPCODE_BIT:
                case OPCODE_SBC:
                case OPCODE_ROR:
                case OPCODE_ROL:
                case OPCODE_LSR:
                case OPCODE_ASL:
                    if(cpu_state.reg_p.m)
                    {
                        width = 2;
                    }
                    else
                    {
                        width = 3;
                    }
                break;

                case OPCODE_LDX:
                case OPCODE_LDY:
                case OPCODE_CPX:
                case OPCODE_CPY:
                    if(cpu_state.reg_p.x)
                    {
                        width = 2;
                    }
                    else
                    {
                        width = 3;
                    }
                break;

                default:
                    width = 2;
                break;
            }
        break;
    }

    return width;
}

char instruction_str_buffer[512];

char *instruction_str(uint32_t effective_address)
{
//    char *opcode_str;
    const char *opcode_str;
    char addr_mode_str[128];
//    char flags_str[32];
    char temp_str[64];
    int32_t width = 0;
    struct opcode_t opcode;

    opcode = opcode_matrix[peek_byte(effective_address)];
    width = opcode_width(&opcode);

    switch(opcode.address_mode)
    {
        case ADDRESS_MODE_ABSOLUTE:
//            strcpy(addr_mode_str, "absolute addr (");
            sprintf(addr_mode_str, "DBR(%02x):addr(%04x)", cpu_state.regs[REG_DBR].word, peek_word(effective_address + 1));
//            strcat(addr_mode_str, temp_str);
            // for(int32_t index = width - 1; index > 0; index--)
            // {
//            sprintf(temp_str, "%04x", peek_word(effective_address + 1));
//            strcat(addr_mode_str, temp_str);
            // }
//            strcat(addr_mode_str, ")");
        break;

        case ADDRESS_MODE_ABSOLUTE_INDEXED_INDIRECT:
        {
//            strcpy(addr_mode_str, "absolute addr ( pointer (");
//            strcat(addr_mode_str, "00:");
//            uint32_t pointer = peek_word(effective_address + 1);
//            sprintf(temp_str, "%04x", pointer);
//            strcat(addr_mode_str, temp_str);
//            strcat(addr_mode_str, ") + ");
//            sprintf(temp_str, "X(%04x) ) = ", cpu_state.regs[REG_X].word);
//            strcat(addr_mode_str, temp_str);
//            sprintf(temp_str, "(%04x)", peek_word(pointer));
//            strcat(addr_mode_str, temp_str);
            uint16_t address = peek_word(effective_address + 1);
            uint16_t target = peek_word(address + cpu_state.regs[REG_X].word);
            if(opcode.opcode == OPCODE_JMP || opcode.opcode == OPCODE_JSR)
            {
                sprintf(addr_mode_str, "[addr(%04x) + X(%04x)] => PBR(%02x):%04x", address, cpu_state.regs[REG_X].word,
                                                                            cpu_state.regs[REG_PBR].byte[0], target);
            }
        }
        break;

        case ADDRESS_MODE_ABSOLUTE_INDEXED_X:
        {
            uint16_t address = peek_word(effective_address + 1);
            sprintf(addr_mode_str, "addr(%04x) + X(%04x) => DBR(%02x):%04x", address, cpu_state.regs[REG_X].word,
                                                        cpu_state.regs[REG_DBR].byte[0], cpu_state.regs[REG_X].word + address);
        }
//            strcpy(addr_mode_str, "absolute addr (");
//            sprintf(temp_str, "DBR(%02x):", cpu_state.regs[REG_DBR].word);
//            strcat(addr_mode_str, temp_str);
//            sprintf(temp_str, "%04x", peek_word(effective_address + 1));
//            strcat(addr_mode_str, temp_str);
//            strcat(addr_mode_str, ") + ");
//            sprintf(temp_str, "X(%04x) )", cpu_state.regs[REG_X].word);
//            strcat(addr_mode_str, temp_str);
        break;

        case ADDRESS_MODE_ABSOLUTE_INDEXED_Y:
        {
            uint16_t address = peek_word(effective_address + 1);
            sprintf(addr_mode_str, "addr(%04x) + Y(%04x) => DBR(%02x):%04x", address, cpu_state.regs[REG_Y].word,
                                                        cpu_state.regs[REG_DBR].byte[0], cpu_state.regs[REG_Y].word + address);
        }
//            strcpy(addr_mode_str, "absolute addr (");
//            sprintf(temp_str, "DBR(%02x):", cpu_state.regs[REG_DBR].word);
//            strcat(addr_mode_str, temp_str);
//            sprintf(temp_str, "%04x", peek_word(effective_address + 1));
//            strcat(addr_mode_str, temp_str);
//            strcat(addr_mode_str, ") + ");
//            sprintf(temp_str, "Y(%04x)", cpu_state.regs[REG_Y].word);
//            strcat(addr_mode_str, temp_str);
        break;

        case ADDRESS_MODE_ABSOLUTE_INDIRECT:
        {
            strcpy(addr_mode_str, "absolute addr ( pointer (");
            uint32_t pointer = peek_word(effective_address + 1);
            // for(int32_t index = width - 1; index > 0; index--)
            // {
            sprintf(temp_str, "%04x", pointer);
            strcat(addr_mode_str, temp_str);
            // }
            strcat(addr_mode_str, ") ) = ");
            sprintf(temp_str, "(%04x)", peek_word(pointer));
            strcat(addr_mode_str, temp_str);
        }
        break;

        case ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X:
        {
            strcpy(addr_mode_str, "absolute long addr (");
            uint32_t pointer = peek_word(effective_address + 1);
            pointer |= (uint32_t)peek_byte(effective_address + 3) << 16;
            // for(int32_t index = width - 1; index > 0; index--)
            // {
            sprintf(temp_str, "%06x", pointer);
            strcat(addr_mode_str, temp_str);
            // }

            strcat(addr_mode_str, ") + ");
            sprintf(temp_str, "X(%04x)", cpu_state.regs[REG_X].word);
            strcat(addr_mode_str, temp_str);
        }
        break;

        case ADDRESS_MODE_ABSOLUTE_LONG:
        {
            uint32_t address = peek_word(effective_address + 1);
            address |= (uint32_t)peek_byte(effective_address + 3) << 16;
            sprintf(addr_mode_str, "addrl(%06x)", address);
//            strcpy(addr_mode_str, "absolute long addr (");
//            uint32_t pointer = peek_word(effective_address + 1);
//            pointer |= (uint32_t)peek_byte(effective_address + 3) << 16;
//            sprintf(temp_str, "%06x", pointer);
//            strcat(addr_mode_str, temp_str);
//            strcat(addr_mode_str, ")");
        }
        break;

        case ADDRESS_MODE_ACCUMULATOR:
            addr_mode_str[0] = '\0';
            if(opcode.opcode != OPCODE_XCE && opcode.opcode != OPCODE_TXS && opcode.opcode != OPCODE_WDM && opcode.opcode != OPCODE_WAI)
            {
                sprintf(addr_mode_str, "accumulator(%04x)", cpu_state.regs[REG_ACCUM].word);
            }
        break;

        case ADDRESS_MODE_BLOCK_MOVE:
            sprintf(addr_mode_str, "dst addr(%02x:%04x), src addr(%02x:%04x)", peek_byte(effective_address + 1), cpu_state.regs[REG_Y].word,
                                                                               peek_byte(effective_address + 2), cpu_state.regs[REG_X].word);
        break;

        case ADDRESS_MODE_DIRECT_INDEXED_INDIRECT:
            strcpy(addr_mode_str, "direct indexed indirect - (d,x)");
        break;

        case ADDRESS_MODE_DIRECT_INDEXED_X:
            sprintf(addr_mode_str, "D(%04x) + X(%04x) => 00:%04x", cpu_state.regs[REG_D].word, cpu_state.regs[REG_X].word,
                                                                   cpu_state.regs[REG_D].word + cpu_state.regs[REG_X].word);
        break;

        case ADDRESS_MODE_DIRECT_INDEXED_Y:
            sprintf(addr_mode_str, "D(%04x) + Y(%04x) => 00:%04x", cpu_state.regs[REG_D].word, cpu_state.regs[REG_Y].word,
                                                                   cpu_state.regs[REG_D].word + cpu_state.regs[REG_Y].word);
        break;

        case ADDRESS_MODE_DIRECT_INDIRECT_INDEXED:
        {
            uint8_t offset = peek_byte(effective_address + 1);
            uint16_t address = peek_word(cpu_state.regs[REG_D].word + offset);
            sprintf(addr_mode_str, "DBR:(%02x):[D(%04x) + offset(%02x)]=(%04x) + Y(%04x)",  cpu_state.regs[REG_DBR].byte[0],
                                                                                            cpu_state.regs[REG_D].word, offset,
                                                                                            address, cpu_state.regs[REG_Y].word);
        }
        break;

        case ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED:
            strcpy(addr_mode_str, "direct indirect long indexed - [d],y");
        break;

        case ADDRESS_MODE_DIRECT_INDIRECT_LONG:
            strcpy(addr_mode_str, "direct indirect long - [d]");
        break;

        case ADDRESS_MODE_DIRECT_INDIRECT:
        {
            uint8_t offset = peek_byte(effective_address + 1);
            uint16_t address = peek_word(cpu_state.regs[REG_D].word + offset);
            sprintf(addr_mode_str, "[D(%04x) + offset(%02x)](%04x) => DBR(%02x):%04x", address, cpu_state.regs[REG_D].word, offset,
                                                                                        cpu_state.regs[REG_DBR].word, address);
        }
        break;

        case ADDRESS_MODE_DIRECT:
        {
            uint8_t offset = peek_byte(effective_address + 1);
            sprintf(addr_mode_str, "D(%04x) + offset(%02x) => 00:%04x", cpu_state.regs[REG_D].word, offset, cpu_state.regs[REG_D].word + offset);
        }
        break;

        case ADDRESS_MODE_IMMEDIATE:
            strcpy(addr_mode_str, "immediate (");

            for(int32_t index = width - 1; index > 0; index--)
            {
                sprintf(temp_str, "%02x", peek_byte(effective_address + index));
                strcat(addr_mode_str, temp_str);
            }
            strcat(addr_mode_str, ")");
        break;

        case ADDRESS_MODE_IMPLIED:
            strcpy(addr_mode_str, "");
        break;

        case ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE_LONG:
            strcpy(addr_mode_str, "program counter relative long - rl");
        break;

        case ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE:
        {
            uint16_t offset = peek_byte(effective_address + 1);
            if(offset & 0x80)
            {
                offset |= 0xff00;
            }
            sprintf(addr_mode_str, "PC(%04x) + offset(%02x) => PBR(%02x):%04x", cpu_state.regs[REG_PC].word + 2, (uint8_t)offset, cpu_state.regs[REG_PBR].byte[0],
                                                                                (uint16_t)((cpu_state.regs[REG_PC].word + 2) + offset));
        }
        break;

        case ADDRESS_MODE_STACK:
            addr_mode_str[0] = '\0';
            if(opcode.opcode == OPCODE_PEA)
            {
                sprintf(temp_str, "immediate (%04x)", peek_word(effective_address + 1));
                strcat(addr_mode_str, temp_str);
            }
        break;

        case ADDRESS_MODE_STACK_RELATIVE:
            sprintf(addr_mode_str, "S(%04x) + offset(%02x)", cpu_state.regs[REG_S].word, peek_byte(effective_address + 1));
        break;

        case ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED:
        {
            uint8_t offset = peek_byte(effective_address + 1);
            uint16_t pointer = peek_word(cpu_state.regs[REG_S].word + offset) + cpu_state.regs[REG_Y].word;
            sprintf(addr_mode_str, "[S(%04x) + offset(%02x)]=(%04x) + Y(%04x)", cpu_state.regs[REG_S].word, offset, pointer, cpu_state.regs[REG_Y].word);
        }
//            strcpy(addr_mode_str, "stack relative indirect indexed - (d,s),y");
        break;

        default:
        case ADDRESS_MODE_UNKNOWN:
            strcpy(addr_mode_str, "unknown");
        break;
    }

    opcode_str = opcode_strs[opcode.opcode];

    sprintf(instruction_str_buffer, "[%02x:%04x]: ", (effective_address >> 16) & 0xff, effective_address & 0xffff);
    uint32_t index;
    for(index = 0; index < width; index++)
    {
        sprintf(temp_str, "%02x ", peek_byte(effective_address + index));
        strcat(instruction_str_buffer, temp_str);
    }

    for(; index < 4; index++)
    {
        strcat(instruction_str_buffer, "   ");
    }
//    uint32_t instruction_bytes = 0;
//    for(int32_t i = 0; i < width; i++)
//    {
//        instruction_bytes <<= 8;
//        instruction_bytes |= peek_byte(effective_address + i);
//    }
//    sprintf(temp_str, "%-8x", instruction_bytes);
//    strcat(instruction_str_buffer, temp_str);

    strcat(instruction_str_buffer, "| ");
    strcat(instruction_str_buffer, opcode_str);

    if(addr_mode_str[0])
    {
        strcat(instruction_str_buffer, " ");
        strcat(instruction_str_buffer, addr_mode_str);
    }

    return instruction_str_buffer;
}

char *instruction_str2(uint32_t effective_address)
{
    //    char *opcode_str;
    char opcode_str[16];
    char addr_mode_str[32] = "";
    char flags_str[32] = "";
    char temp_str[32] = "";
    char regs_str[64] = "";
    int32_t width = 0;
    struct opcode_t opcode;
    uint32_t show_computed_address = 0;
    uint32_t computed_effective_address = 0;

    opcode = opcode_matrix[peek_byte(effective_address)];
    width = opcode_width(&opcode);

    switch(opcode.address_mode)
    {
        case ADDRESS_MODE_ABSOLUTE:
        {
            uint16_t address = peek_word(effective_address + 1);
            sprintf(addr_mode_str, "$%04x", address);
            computed_effective_address = ((uint32_t)cpu_state.regs[REG_DBR].byte[0] << 16) | address;
            show_computed_address = 1;
        }
        break;

        case ADDRESS_MODE_ABSOLUTE_INDEXED_INDIRECT:
        {
//            uint16_t address = peek_word(effective_address + 1);
//            uint16_t target = peek_word(address + cpu_state.regs[REG_X].word);
//            if(opcode.opcode == OPCODE_JMP)
//            {
//
//                sprintf(addr_mode_str, "[addr(%04x) + X(%04x)] => PBR(%02x):%04x", address, cpu_state.regs[REG_X].word,
//                                                                            cpu_state.regs[REG_PBR].byte[0], target);
//            }
        }
        break;

        case ADDRESS_MODE_ABSOLUTE_INDEXED_X:
        {
            show_computed_address = 1;
            uint16_t address = peek_word(effective_address + 1);
            sprintf(addr_mode_str, "$%04x,x", address);
            computed_effective_address = (((uint32_t)cpu_state.regs[REG_DBR].byte[0] << 16) | address) + cpu_state.regs[REG_X].word;
        }
        break;

        case ADDRESS_MODE_ABSOLUTE_INDEXED_Y:
        {
            show_computed_address = 1;
            uint16_t address = peek_word(effective_address + 1);
            sprintf(addr_mode_str, "$%04x,y", address);
            computed_effective_address = (((uint32_t)cpu_state.regs[REG_DBR].byte[0] << 16) | address) + cpu_state.regs[REG_Y].word;
        }
        break;

        case ADDRESS_MODE_ABSOLUTE_INDIRECT:
        {
//            strcpy(addr_mode_str, "absolute addr ( pointer (");
//            uint32_t pointer = peek_word(effective_address + 1);
//            // for(int32_t index = width - 1; index > 0; index--)
//            // {
//            sprintf(temp_str, "%04x", pointer);
//            strcat(addr_mode_str, temp_str);
//            // }
//            strcat(addr_mode_str, ") ) = ");
//            sprintf(temp_str, "(%04x)", peek_word(pointer));
//            strcat(addr_mode_str, temp_str);
        }
        break;

        case ADDRESS_MODE_ABSOLUTE_LONG_INDEXED_X:
        {
            show_computed_address = 1;
            uint32_t address = peek_word(effective_address + 1);
            address |= (uint32_t)peek_byte(effective_address + 3) << 16;
            sprintf(addr_mode_str, "$%06x,x", address);
            computed_effective_address = (((uint32_t)cpu_state.regs[REG_DBR].byte[0] << 16) | address) + cpu_state.regs[REG_X].word;
        }
        break;

        case ADDRESS_MODE_ABSOLUTE_LONG:
        {
            show_computed_address = 1;
            uint32_t address = peek_word(effective_address + 1);
            address |= (uint32_t)peek_byte(effective_address + 3) << 16;
            sprintf(addr_mode_str, "$%06x", address);
            computed_effective_address = ((uint32_t)cpu_state.regs[REG_DBR].byte[0] << 16) | address;
        }
        break;

        case ADDRESS_MODE_ACCUMULATOR:
            // addr_mode_str[0] = '\0';
            if(opcode.opcode != OPCODE_XCE && opcode.opcode != OPCODE_TXS && opcode.opcode != OPCODE_WDM && opcode.opcode != OPCODE_WAI)
            {
                // sprintf(addr_mode_str, "accumulator(%04x)", cpu_state.regs[REG_ACCUM].word);
                strcpy(addr_mode_str, "a");
            }
        break;

        case ADDRESS_MODE_BLOCK_MOVE:
            // sprintf(addr_mode_str, "dst addr(%02x:%04x), src addr(%02x:%04x)", peek_byte(effective_address + 1), cpu_state.regs[REG_Y].word,
            //                                                                    peek_byte(effective_address + 2), cpu_state.regs[REG_X].word);
        break;

        case ADDRESS_MODE_DIRECT_INDEXED_INDIRECT:
            // strcpy(addr_mode_str, "direct indexed indirect - (d,x)");
        break;

        case ADDRESS_MODE_DIRECT_INDEXED_X:
        {
            uint8_t offset = peek_byte(effective_address + 1);
            sprintf(addr_mode_str, "$%02x,x", offset);
            computed_effective_address = (cpu_state.regs[REG_D].word + offset) + cpu_state.regs[REG_X].word;
            show_computed_address = 1;
        }
        break;

        case ADDRESS_MODE_DIRECT_INDEXED_Y:
        {
            uint8_t offset = peek_byte(effective_address + 1);
            sprintf(addr_mode_str, "$%02x,y", offset);
            computed_effective_address = (cpu_state.regs[REG_D].word + offset) + cpu_state.regs[REG_Y].word;
            show_computed_address = 1;
        }
        break;

        case ADDRESS_MODE_DIRECT_INDIRECT_INDEXED:
        // {
        //     uint8_t offset = peek_byte(effective_address + 1);
        //     uint16_t address = peek_word(cpu_state.regs[REG_D].word + offset);
        //     sprintf(addr_mode_str, "DBR:(%02x):[D(%04x) + offset(%02x)]=(%04x) + Y(%04x)",  cpu_state.regs[REG_DBR].byte[0],
        //                                                                                     cpu_state.regs[REG_D].word, offset,
        //                                                                                     address, cpu_state.regs[REG_Y].word);
        // }
        break;

        case ADDRESS_MODE_DIRECT_INDIRECT_LONG_INDEXED:
            // strcpy(addr_mode_str, "direct indirect long indexed - [d],y");
        break;

        case ADDRESS_MODE_DIRECT_INDIRECT_LONG:
            // strcpy(addr_mode_str, "direct indirect long - [d]");
        break;

        case ADDRESS_MODE_DIRECT_INDIRECT:
        // {
        //     uint8_t offset = peek_byte(effective_address + 1);
        //     uint16_t address = peek_word(cpu_state.regs[REG_D].word + offset);
        //     sprintf(addr_mode_str, "[D(%04x) + offset(%02x)](%04x) => DBR(%02x):%04x", address, cpu_state.regs[REG_D].word, offset,
        //                                                                                 cpu_state.regs[REG_DBR].word, address);
        // }
        break;

        case ADDRESS_MODE_DIRECT:
        {
            uint8_t offset = peek_byte(effective_address + 1);
            sprintf(addr_mode_str, "$%02x", offset);
            computed_effective_address = cpu_state.regs[REG_D].word + offset;
            show_computed_address = 1;
        }
        // {
        //     uint8_t offset = peek_byte(effective_address + 1);
        //     sprintf(addr_mode_str, "D(%04x) + offset(%02x) => 00:%04x", cpu_state.regs[REG_D].word, offset, cpu_state.regs[REG_D].word + offset);
        // }
        break;

        case ADDRESS_MODE_IMMEDIATE:
        {
            strcpy(addr_mode_str, "#$");
//            uint32_t immediate = 0;
            for(int32_t index = width - 1; index > 0; index--)
            {
                sprintf(temp_str, "%02x", peek_byte(effective_address + index));
                strcat(addr_mode_str, temp_str);
//                immediate <<= 8;
//                immediate |= peek_byte(effective_address + index);
            }

//            sprintf(addr_mode_str, "#$%04x", immediate);
        }
        break;

        case ADDRESS_MODE_IMPLIED:
            // strcpy(addr_mode_str, "");
        break;

        case ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE_LONG:
            // strcpy(addr_mode_str, "program counter relative long - rl");
        break;

        case ADDRESS_MODE_PROGRAM_COUNTER_RELATIVE:
        {
            uint16_t offset = peek_byte(effective_address + 1);
            if(offset & 0x80)
            {
                offset |= 0xff00;
            }

            computed_effective_address = ((uint32_t)cpu_state.regs[REG_PBR].word << 16) | ((cpu_state.regs[REG_PC].word + offset + 2) & 0xffff);
            show_computed_address = 1;
            sprintf(addr_mode_str, "$%04x", computed_effective_address & 0xffff);
        //     sprintf(addr_mode_str, "PC(%04x) + offset(%02x) => PBR(%02x):%04x", cpu_state.regs[REG_PC].word + 2, (uint8_t)offset, cpu_state.regs[REG_PBR].byte[0],
        //                                                                         (uint16_t)((cpu_state.regs[REG_PC].word + 2) + offset));
        }
        break;

        case ADDRESS_MODE_STACK:
            // addr_mode_str[0] = '\0';
            // if(opcode.opcode == OPCODE_PEA)
            // {
            //     sprintf(temp_str, "immediate (%04x)", peek_word(effective_address + 1));
            //     strcat(addr_mode_str, temp_str);
            // }
        break;

        case ADDRESS_MODE_STACK_RELATIVE:
        {
            uint16_t offset = peek_byte(effective_address + 1);
            if(offset & 0x80)
            {
                offset |= 0xff00;
            }
            computed_effective_address = cpu_state.regs[REG_S].word + offset;
            show_computed_address = 1;
            sprintf(addr_mode_str, "$%02x,s", offset);
        }
            // sprintf(addr_mode_str, "S(%04x) + offset(%02x)", cpu_state.regs[REG_S].word, peek_byte(effective_address + 1));
        break;

        case ADDRESS_MODE_STACK_RELATIVE_INDIRECT_INDEXED:
        // {
        //     uint8_t offset = peek_byte(effective_address + 1);
        //     uint16_t pointer = peek_word(cpu_state.regs[REG_S].word + offset) + cpu_state.regs[REG_Y].word;
        //     sprintf(addr_mode_str, "[S(%04x) + offset(%02x)]=(%04x) + Y(%04x)", cpu_state.regs[REG_S].word, offset, pointer, cpu_state.regs[REG_Y].word);
        // }
//            strcpy(addr_mode_str, "stack relative indirect indexed - (d,s),y");
        break;

        default:
        // case ADDRESS_MODE_UNKNOWN:
        //     strcpy(addr_mode_str, "unknown");
        // break;
    }

    uint32_t addr_mode_str_len = strlen(addr_mode_str);
    while(addr_mode_str_len < 18)
    {
        addr_mode_str[addr_mode_str_len] = ' ';
        addr_mode_str_len++;
    }

    addr_mode_str[addr_mode_str_len] = '\0';

    // opcode_str = opcode_strs[opcode.opcode];

    uint32_t index = 0;
    while(opcode_strs[opcode.opcode][index])
    {
        opcode_str[index] = tolower(opcode_strs[opcode.opcode][index]);
        index++;
    }
    opcode_str[index] = '\0';

    if(show_computed_address)
    {
        sprintf(addr_mode_str + 10, "[%06x]", computed_effective_address);
    }

    if(cpu_state.reg_p.e)
    {
        sprintf(flags_str, "%c%c1%c%c%c%c%c", cpu_state.reg_p.n ? 'N' : 'n',
                                               cpu_state.reg_p.v ? 'V' : 'v',
                                               cpu_state.reg_p.b ? 'B' : 'b',
                                               cpu_state.reg_p.d ? 'D' : 'd',
                                               cpu_state.reg_p.i ? 'I' : 'i',
                                               cpu_state.reg_p.z ? 'Z' : 'z',
                                               cpu_state.reg_p.c ? 'C' : 'c');
    }
    else
    {
        sprintf(flags_str, "%c%c%c%c%c%c%c%c", cpu_state.reg_p.n ? 'N' : 'n',
                                               cpu_state.reg_p.v ? 'V' : 'v',
                                               cpu_state.reg_p.m ? 'M' : 'm',
                                               cpu_state.reg_p.x ? 'X' : 'x',
                                               cpu_state.reg_p.d ? 'D' : 'd',
                                               cpu_state.reg_p.i ? 'I' : 'i',
                                               cpu_state.reg_p.z ? 'Z' : 'z',
                                               cpu_state.reg_p.c ? 'C' : 'c');
    }

    sprintf(regs_str, "A:%04x X:%04x Y:%04x S:%04x D:%04x DB:%02x %s", cpu_state.regs[REG_ACCUM].word,
                                                                        cpu_state.regs[REG_X].word,
                                                                        cpu_state.regs[REG_Y].word,
                                                                        cpu_state.regs[REG_S].word,
                                                                        cpu_state.regs[REG_D].word,
                                                                        cpu_state.regs[REG_DBR].byte[0], flags_str);

    sprintf(instruction_str_buffer, "%06x %s %s %s V:%3d H:%4d", effective_address, opcode_str, addr_mode_str, regs_str, vcounter, scanline_cycles);

    return instruction_str_buffer;
}

int dump_cpu(int show_registers)
{
    uint32_t address;
    int opcode_offset;
    struct opcode_t opcode;
    char *op_str;
    unsigned char *opcode_address;
    address = cpu_state.instruction_address;

//    op_str = instruction_str(address);
    op_str = instruction_str(address);

    printf("===================== CPU ========================\n");

    if(show_registers)
    {
        // printf("=========== REGISTERS ===========\n");
        printf("[A: %04x] | ", cpu_state.regs[REG_ACCUM].word);
        printf("[X: %04x] | ", cpu_state.regs[REG_X].word);
        printf("[Y: %04x] | ", cpu_state.regs[REG_Y].word);
        printf("[S: %04x] | ", cpu_state.regs[REG_S].word);
        printf("[D: %04x]\n", cpu_state.regs[REG_D].word);
        printf("[DBR: %02x] | ", cpu_state.regs[REG_DBR].byte[0]);
        printf("[PBR: %02x] | ", cpu_state.regs[REG_PBR].byte[0]);

        printf("[P: N:%d V:%d ", cpu_state.reg_p.n, cpu_state.reg_p.v);

        if(cpu_state.reg_p.e)
        {
            printf("B:%d ", cpu_state.reg_p.b);
        }
        else
        {
            printf("M:%d X:%d ", cpu_state.reg_p.m, cpu_state.reg_p.x);
        }

        printf("D:%d Z:%d I:%d C:%d]\n", cpu_state.reg_p.d, cpu_state.reg_p.z, cpu_state.reg_p.i, cpu_state.reg_p.c);
        printf("  [E: %02x] | [PC: %04x]", cpu_state.reg_p.e, cpu_state.regs[REG_PC].word);

        if(cpu_state.regs[REG_INST].word == BRK_S || cpu_state.regs[REG_INST].word == INT_HW)
        {
            char *interrupt_str = "";
            switch(cpu_state.cur_interrupt)
            {
                case CPU_INT_BRK:
                    interrupt_str = "BRK";
                break;

                case CPU_INT_IRQ:
                    interrupt_str = "IRQ";
                break;

                case CPU_INT_NMI:
                    interrupt_str = "NMI";
                break;

                case CPU_INT_COP:
                    interrupt_str = "COP";
                break;

                case CPU_INT_RES:
                    interrupt_str = "RES";
                break;
            }

            printf(" | handling %s", interrupt_str);
        }
        printf("\n");

        // printf("=========== REGISTERS ===========\n");
        printf("-------------------------------------\n");
    }

    printf("%s\n", op_str);

    printf("\n");

    return 0;
}

int view_hardware_registers()
{
    // int i;

    // for(i = 0; i < sizeof(hardware_regs.hardware_regs); i++)
    // {
    //     printf("<0x%04x>: 0x%02x\n", CPU_REGS_START_ADDRESS + i, hardware_regs.hardware_regs[i]);
    // }
}

uint32_t cpu_mem_cycles(uint32_t effective_address)
{
    uint32_t offset = effective_address & 0xffff;
    uint32_t bank = (effective_address >> 16) & 0xff;

    uint32_t bank_region1 = bank >= 0x00 && bank <= 0x3f;
    uint32_t bank_region2 = bank >= 0x80 && bank <= 0xbf;

    if(bank_region1 || bank_region2)
    {
        uint32_t index;
        index =  offset < 0x8000;
        index += offset < 0x6000;
        index += offset < 0x4200;
        index += offset < 0x4000;
        index += offset < 0x2000;

        return mem_speed_map[index][bank_region2 && (ram1_regs[CPU_REG_MEMSEL] & 1)];
    }
    else if(ram1_regs[CPU_REG_MEMSEL] & 1)
    {
        return MEM_SPEED_FAST_CYCLES;
    }


    return MEM_SPEED_MED_CYCLES;
}

void cpu_write_byte(uint32_t effective_address, uint8_t data)
{
//    cpu_cycle_count += ACCESS_CYCLES(effective_address);
    // cpu_cycle_count += cpu_mem_cycles(effective_address);
    write_byte(effective_address, data);
}

void cpu_write_word(uint32_t effective_address, uint16_t data)
{
    cpu_write_byte(effective_address, data);
    cpu_write_byte(effective_address + 1, (data >> 8) & 0xff);
}

uint8_t cpu_read_byte(uint32_t effective_address)
{
//    cpu_cycle_count += ACCESS_CYCLES(effective_address);
    // cpu_cycle_count += cpu_mem_cycles(effective_address);
    return read_byte(effective_address);
}

uint16_t cpu_read_word(uint32_t effective_address)
{
    return (uint16_t)cpu_read_byte(effective_address) | ((uint16_t)cpu_read_byte(effective_address + 1) << 8);
}

uint32_t cpu_read_wordbyte(uint32_t effective_address)
{
    return (uint32_t)cpu_read_word(effective_address) | ((uint32_t)cpu_read_byte(effective_address + 2) << 16);
}

void assert_nmi(uint8_t bit)
{
    cpu_state.nmi1 = cpu_state.nmi0;

    if(!cpu_state.nmi1)
    {
        cpu_state.interrupts[CPU_INT_NMI] = 1;
    }

    cpu_state.nmi0 |= 1 << bit;
}

void deassert_nmi(uint8_t bit)
{
    cpu_state.nmi0 = cpu_state.nmi1;
    cpu_state.nmi1 &= ~(1 << bit);
}

void assert_irq(uint8_t bit)
{
    cpu_state.irq |= 1 << bit;
}

void deassert_irq(uint8_t bit)
{
    cpu_state.irq &= ~(1 << bit);
}

void assert_rdy(uint8_t bit)
{
    if(!cpu_state.wai && !cpu_state.stp)
    {
        cpu_state.rdy |= 1 << bit;
    }
}

void deassert_rdy(uint8_t bit)
{
    cpu_state.rdy &= ~(1 << bit);
}

void reset_cpu()
{
    reset_core();
    /* elusive CPU delay at startup. Ripped off from bsnes. Wish I
    knew why this is needed... */
    cpu_state.uop_cycles = -22 * CPU_MASTER_CYCLES;

    ram1_regs[CPU_REG_MEMSEL] = 0;
    ram1_regs[CPU_REG_TIMEUP] = 0;
    ram1_regs[CPU_REG_HTIMEL] = 0xff;
    ram1_regs[CPU_REG_HTIMEH] = 0x01;
    ram1_regs[CPU_REG_VTIMEL] = 0xff;
    ram1_regs[CPU_REG_VTIMEH] = 0x01;
    ram1_regs[CPU_REG_MDMAEN] = 0;
    ram1_regs[CPU_REG_HDMAEN] = 0;
}

void reset_core()
{
    cpu_state.regs[REG_PC].word = 0xfffc;
    cpu_state.regs[REG_DBR].byte[0] = 0;
    cpu_state.regs[REG_PBR].byte[0] = 0;
    cpu_state.regs[REG_D].word = 0;
    cpu_state.regs[REG_X].byte[1] = 0;
    cpu_state.regs[REG_Y].byte[1] = 0;
    cpu_state.regs[REG_S].byte[1] = 0x01;
    cpu_state.regs[REG_ZERO].word = 0;
    cpu_state.reg_p.e = 1;
    cpu_state.reg_p.i = 1;
    cpu_state.reg_p.d = 0;
    cpu_state.reg_p.x = 1;
    cpu_state.reg_p.m = 1;

    cpu_state.interrupts[CPU_INT_BRK] = 0;
    cpu_state.interrupts[CPU_INT_IRQ] = 0;
    cpu_state.interrupts[CPU_INT_NMI] = 0;
    cpu_state.nmi0 = 0;
    cpu_state.nmi1 = 0;
    cpu_state.irq = 0;
    cpu_state.rdy = 1;
    cpu_state.res = 0;
    cpu_state.stp = 0;

    cpu_state.cur_interrupt = CPU_INT_RES;
    cpu_state.interrupts[cpu_state.cur_interrupt] = 1;
    cpu_state.instruction_address = EFFECTIVE_ADDRESS(cpu_state.regs[REG_PBR].byte[0], cpu_state.regs[REG_PC].word);
    cpu_state.regs[REG_INST].word = INT_HW;
    load_instruction();
}

// void assert_pin(uint32_t pin)
// {
//     cpu_state.pins[pin] = 0;

//     switch(pin)
//     {
// //        case CPU_PIN_IRQ:
// //            cpu_state.interrupts[CPU_INT_IRQ] = 1;
// //        break;

//         case CPU_PIN_NMI:
//             cpu_state.interrupts[CPU_INT_NMI] = 1;
//         break;
//     }
// }

// void deassert_pin(uint32_t pin)
// {
//     if(pin == CPU_PIN_RDY && cpu_state.wai)
//     {
//         return;
//     }

//     cpu_state.pins[pin] = 1;
// }

void load_instruction()
{
    cpu_state.instruction = instructions + cpu_state.regs[REG_INST].word;
    cpu_state.uop_index = 0;
    load_uop();

    if(!cpu_state.instruction->uops[0].func)
    {
        unimplemented(0);
    }
}

void load_uop()
{
    cpu_state.uop = cpu_state.instruction->uops + cpu_state.uop_index;
    cpu_state.last_uop = cpu_state.instruction->uops[cpu_state.uop_index + 1].func == NULL;
}

void next_uop()
{
    cpu_state.uop_index++;
    load_uop();
}

uint32_t step_cpu(int32_t *cycle_count)
{
    uint32_t end_of_instruction = 0;
    uint32_t run_step = cpu_state.rdy & (!cpu_state.stp);
    if(run_step)
    {
        cpu_state.uop_cycles += *cycle_count;
        cpu_state.instruction_cycles += *cycle_count;

        while(cpu_state.uop->func)
        {
            int32_t prev_uop_cycles = cpu_state.uop_cycles;

            cpu_state.run_mul = cpu_state.mul_cycles > 0;
            cpu_state.run_div = cpu_state.div_cycles > 0;

            if(cpu_state.last_uop)
            {
                if(cpu_state.irq && !cpu_state.reg_p.i && !cpu_state.interrupts[CPU_INT_NMI])
                {
                    cpu_state.interrupts[CPU_INT_IRQ] = 1;
                }
            }

            if(!cpu_state.uop->func(cpu_state.uop->arg))
            {
                break;
            }

            uint32_t spent_cycle = (cpu_state.uop_cycles - prev_uop_cycles) != 0;

            if(spent_cycle)
            {
                if(cpu_state.run_mul)
                {
                    cpu_state.mul_cycles--;
                    uint16_t current_product = (uint16_t)ram1_regs[CPU_REG_RDMPYL] | ((uint16_t)ram1_regs[CPU_REG_RDMPYH] << 8);
                    uint32_t shift = CPU_MUL_MACHINE_CYCLES - cpu_state.mul_cycles;
                    uint16_t quotient = (uint16_t)ram1_regs[CPU_REG_RDDIVL] | ((uint16_t)ram1_regs[CPU_REG_RDDIVH] << 8);

                    if(quotient & 0x01)
                    {
                        current_product += cpu_state.shifter;
                    }

                    quotient >>= 1;
                    cpu_state.shifter <<= 1;

                    ram1_regs[CPU_REG_RDMPYL] = current_product & 0xff;
                    ram1_regs[CPU_REG_RDMPYH] = (current_product >> 8) & 0xff;
                    ram1_regs[CPU_REG_RDDIVL] = quotient & 0xff;
                    ram1_regs[CPU_REG_RDDIVH] = (quotient >> 8) & 0xff;
                }

                if(cpu_state.run_div)
                {
                    cpu_state.div_cycles--;
                    uint16_t current_quotient = (uint16_t)ram1_regs[CPU_REG_RDDIVL] | ((uint16_t)ram1_regs[CPU_REG_RDDIVH] << 8);
                    uint16_t product = (uint16_t)ram1_regs[CPU_REG_RDMPYL] | ((uint16_t)ram1_regs[CPU_REG_RDMPYH] << 8);
                    current_quotient <<= 1;
                    cpu_state.shifter >>= 1;
                    if(product >= cpu_state.shifter)
                    {
                        product -= cpu_state.shifter;
                        current_quotient |= 1;
                    }

                    ram1_regs[CPU_REG_RDDIVL] = current_quotient & 0xff;
                    ram1_regs[CPU_REG_RDDIVH] = (current_quotient >> 8) & 0xff;
                    ram1_regs[CPU_REG_RDMPYL] = product & 0xff;
                    ram1_regs[CPU_REG_RDMPYH] = (product >> 8) & 0xff;
                }
            }

            cpu_state.reg_p.dl = cpu_state.regs[REG_D].byte[0] != 0;
            cpu_state.reg_p.am = cpu_state.regs[REG_ACCUM].word == 0xffff;
            next_uop();
        }
    }
    else if(cpu_state.wai && cpu_state.irq && !cpu_state.interrupts[CPU_INT_NMI])
    {
        cpu_state.interrupts[CPU_INT_IRQ] = 1;
    }

    if(!cpu_state.uop->func)
    {
        if(cpu_state.interrupts[CPU_INT_NMI])
        {
            cpu_state.rdy = 1;
            cpu_state.wai = 0;
            cpu_state.regs[REG_INST].word = INT_HW;
            cpu_state.cur_interrupt = CPU_INT_NMI;
            cpu_state.interrupts[CPU_INT_NMI] = 0;
        }
        else if(cpu_state.interrupts[CPU_INT_IRQ])
        {
            if(cpu_state.reg_p.i)
            {
                cpu_state.regs[REG_INST].word = FETCH;
                cpu_state.cur_interrupt = CPU_INT_BRK;
            }
            else
            {
                cpu_state.regs[REG_INST].word = INT_HW;
                cpu_state.cur_interrupt = CPU_INT_IRQ;
            }

            cpu_state.rdy = 1;
            cpu_state.wai = 0;
            cpu_state.interrupts[CPU_INT_IRQ] = 0;
        }
        else
        {
            cpu_state.regs[REG_INST].word = FETCH;
            cpu_state.cur_interrupt = CPU_INT_BRK;
        }

        if(!cpu_state.wai)
        {
            *cycle_count -= cpu_state.uop_cycles;
            cpu_state.uop_cycles = 0;
            // cpu_state.interrupts[cpu_state.cur_interrupt] = 0;
            cpu_state.instruction_cycles = cpu_state.uop_cycles;
            cpu_state.instruction_address = EFFECTIVE_ADDRESS(cpu_state.regs[REG_PBR].byte[0], cpu_state.regs[REG_PC].word);
            cpu_state.uop_index = 0;
            load_instruction();
        }

        end_of_instruction = 1;
    }

    return end_of_instruction;
}

void init_disasm(struct disasm_state_t *disasm_state, struct cpu_state_t *cpu_state)
{
    // disasm_state->reg_pc = cpu_state->regs[REG_PC].word;
    // disasm_state->reg_pbr = cpu_state->regs[REG_PBR].byte[0];
    disasm_state->reg_pc = cpu_state->instruction_address & 0xffff;
    disasm_state->reg_pbr = (cpu_state->instruction_address >> 16) & 0xff;
    disasm_state->reg_p = (((uint32_t)cpu_state->reg_p.m) << STATUS_FLAG_M) | 
                          (((uint32_t)cpu_state->reg_p.x) << STATUS_FLAG_X) | 
                          (((uint32_t)cpu_state->reg_p.e) << STATUS_FLAG_E);
}

uint32_t disasm(struct disasm_state_t *disasm_state, struct disasm_inst_t *instruction)
{
    uint32_t effective_address = EFFECTIVE_ADDRESS(disasm_state->reg_pbr, disasm_state->reg_pc);
    uint8_t opcode = peek_byte(effective_address);
    struct opcode_info_t *info = opcode_info + opcode;

    instruction->bytes[0] = opcode;
    instruction->width = info->width[(disasm_state->reg_p & (1 << info->width_flag)) && 1];
    instruction->opcode_str = opcode_strs[info->opcode];
    instruction->addr_mode_str = addr_mode_strs[info->addr_mode];

    for(uint32_t index = 1; index < instruction->width; index++)
    {
        effective_address = EFFECTIVE_ADDRESS(disasm_state->reg_pbr, disasm_state->reg_pc + index);
        instruction->bytes[index] = peek_byte(effective_address);;
    }

    disasm_state->reg_pc += instruction->width;
    return 1;
}

// void disasm(struct disasm_state_t *disasm_state, uint32_t instruction_count)
// {
//     while(instruction_count)
//     {
//         uint32_t effective_address = EFFECTIVE_ADDRESS(disasm_state->reg_pbr, disasm_state->reg_pc);
//         uint8_t opcode = peek_byte(effective_address);
//         struct opcode_info_t *info = opcode_info + opcode;
//         printf("[%02x:%04x]: %s\n", disasm_state->reg_pbr, disasm_state->reg_pc, opcode_strs[info->opcode]);
//         disasm_state->reg_pc += info->width[(disasm_state->reg_p & info->width_flag) && 1];
//         instruction_count--;
//     }
// }

uint8_t io_read(uint32_t effective_address)
{
    // printf("io read\n");
}

void io_write(uint32_t effective_address, uint8_t value)
{
    // printf("io write\n");
}

void nmitimen_write(uint32_t effective_address, uint8_t value)
{
    ram1_regs[CPU_REG_NMITIMEN] = value;

    if(value & (CPU_NMITIMEN_FLAG_HTIMER_EN | CPU_NMITIMEN_FLAG_VTIMER_EN))
    {
        update_irq_counter();
    }
    else
    {
        ram1_regs[CPU_REG_TIMEUP] = 0;
    }
}

uint8_t timeup_read(uint32_t effective_address)
{
    uint8_t value = ram1_regs[CPU_REG_TIMEUP];
    ram1_regs[CPU_REG_TIMEUP] = 0;
    /* bits 6-0 are open bus, so we put the last thing that was on the data bus in them */
    return value | (last_bus_value & 0x7f);
}

uint8_t rdnmi_read(uint32_t effective_address)
{
    uint8_t value = ram1_regs[CPU_REG_RDNMI];
    ram1_regs[CPU_REG_RDNMI] &= ~CPU_RDNMI_BLANK_NMI;
    /* bits 6-4 are open bus, so we put the last thing that was on the data bus in them */
    return value | (last_bus_value & 0x70);
}

uint8_t hvbjoy_read(uint32_t effective_address)
{
    uint8_t value = ram1_regs[CPU_REG_HVBJOY];
    /* bits 5-1 are open bus, so we put the last thing that was on the data bus in them */
    return value | (last_bus_value & 0x3e);
}

void wrmpyb_write(uint32_t effective_address, uint8_t value)
{
    if(cpu_state.mul_cycles == 0)
    {
        ram1_regs[CPU_REG_WRMPYB] = value;
        cpu_state.shifter = ram1_regs[CPU_REG_WRMPYB];
        ram1_regs[CPU_REG_RDDIVL] = ram1_regs[CPU_REG_WRMPYA];
        ram1_regs[CPU_REG_RDDIVH] = ram1_regs[CPU_REG_WRMPYB];
        cpu_state.mul_cycles = CPU_MUL_MACHINE_CYCLES;
        /* setting this to zero here guarantees there will be a delay of a single
        machine cycle before the multiplication begins, which is necessary for timing */
        cpu_state.run_mul = 0;
    }

    ram1_regs[CPU_REG_RDMPYL] = 0;
    ram1_regs[CPU_REG_RDMPYH] = 0;
}

void wrdivb_write(uint32_t effective_address, uint8_t value)
{
    if(cpu_state.div_cycles == 0)
    {
        ram1_regs[CPU_REG_WRDIVB] = value;
        cpu_state.shifter = (uint32_t)ram1_regs[CPU_REG_WRDIVB] << 16;
        cpu_state.div_cycles = CPU_DIV_MACHINE_CYCLES;
        /* setting this to zero here guarantees there will be a delay of a single
        machine cycle before the division begins, which is necessary for timing */
        cpu_state.run_div = 0;
//        cpu_state.latched_dividend = (uint16_t)ram1_regs[CPU_REG_WRDIVL] | ((uint16_t)ram1_regs[CPU_REG_WRDIVH] << 8);
    }

    ram1_regs[CPU_REG_RDMPYL] = ram1_regs[CPU_REG_WRDIVL];
    ram1_regs[CPU_REG_RDMPYH] = ram1_regs[CPU_REG_WRDIVH];
//    ram1_regs[CPU_REG_RDMPYL] = cpu_state.latched_dividend & 0xff;
//    ram1_regs[CPU_REG_RDMPYH] = (cpu_state.latched_dividend >> 8) & 0xff;
}








